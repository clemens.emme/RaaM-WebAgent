const PROXY_CONFIG = [
  {
    context: [
      "/git"
    ],
    target: "http://141.51.115.105:7070",
    secure: false,
    changeOrigin: true
  },
  {
  context: [
    "/api"
  ],
    target: "https://raam.uniks.de",
    secure: false,
    changeOrigin: true
  }

];

module.exports = PROXY_CONFIG;
