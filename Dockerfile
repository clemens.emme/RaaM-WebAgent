FROM jfyne/node-alpine-yarn:latest as builder
WORKDIR /usr/src/app
COPY . .
RUN yarn install --frozen-lockfile
RUN yarn unravel

FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/

EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]

