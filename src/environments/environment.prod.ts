export const environment = {
  production: true,
  test: false,

  // GitAgent
  gitServiceURL: 'http://141.51.115.105:7070/git',
  add: '/add',
  repo: '/repo',
  history: '/history',
  branch: '/branch',
  files: '/files',
  commit: '/commit',

  // DBAgent
  dbServiceURL: 'https://raam.uniks.de/api',
  role: '/role',
  task: '/task',
  person: '/person',
  workLog: '/workLogs',
  taskType: '/taskType',
  container: '/container',
  repository: '/repository',

  login: '/login',

  // container
  apiPrefix: '/gen',
  contGenerate: '/generate',

  // default colors
  epicColor: '#9a6324',
  storyColor: '#28a745',
  subTaskColor: '#007bff',
  bugColor: '#d9534f',
  assetColor: '#fd7e14',

  developerColor: '#007bff',
  artistColor: '#fd7e14',
};
