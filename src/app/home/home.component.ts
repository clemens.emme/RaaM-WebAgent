import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GitService} from '../api/services/git/git-service.service';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';
import {UserService} from '../api/services/user/user.service';
import {Person} from '../api/model/person';
import {RepositoryService} from '../api/services/repository/repository.service';
import {MenuBarService} from '../api/services/actionBar/menu-bar.service';
import {environment} from '../../environments/environment';
import {saveAs} from 'file-saver';
import {Repository} from '../api/model/repository';
import {NewCredentialsModalComponent} from '../shared/modals/new-credentials-modal/new-credentials-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  public readonly LOGGED_IN = 'loggedIn';
  public readonly LOGGED_OUT = 'loggedOut';
  public readonly HOME_SECRET_ACTIONS = [
    {text: 'login', call: () => this.fetchLoginData()},
  ];
  public readonly HOME_ACTIONS = [
    {title: '', text: 'Repository Selection', method: '/home', current: true},
    {title: '', text: 'Asset Container', method: '/container'},
    {title: '', text: 'Users', method: '/users'},
  ];


  public test = environment.test;
  @ViewChild('credentialModal') credentialModal: NewCredentialsModalComponent;


  // User Data
  public user: Person;
  public loginData = {
    username: '',
    password: '',
  };

  // Git Data
  public gitURL = '';
  public repoNameList: string[];
  public repoList: Repository[];
  public combinedList: any[];

  constructor(
    public menuBarService: MenuBarService,
    public userService: UserService,
    public snackBar: MatSnackBar,
    public gitService: GitService,
    public repoService: RepositoryService,
    public router: Router
  ) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.HOME_ACTIONS);
    this.menuBarService.setSecretActions(this.HOME_SECRET_ACTIONS);
    this.registerListener();

    if (this.userService.getUser()) {
      this.getUser();
    }

    if (this.test) this.fetchLoginData();
  }

  ngOnDestroy() {
    this.userService.unregisterListener('Home');
  }

  // ==================================================================================================================================
  // Listener Methods
  // ==================================================================================================================================

  private registerListener() {
    this.userService.registerListener('Home', () => this.getUser());
  }

  public getUser() {
    this.user = this.userService.getUser();
    this.getAllInfos();
  }

  // ==================================================================================================================================
  // User Login Methods
  // ==================================================================================================================================

  public login() {
    this.userService.login(this.loginData).then(res => {
      this.user = res;
    });
  }

  public register() {
    const holder = new Person();
    holder.username = this.loginData.username;
    holder.setFirstPassword(this.loginData.password);

    if (!holder.canRegister()) {
      this.openSnack('Info for registration is missing');
      return;
    }

    this.userService.postOne(holder).then(res => {
      console.log(res);
      this.user = res;
      this.userService.setUser(res);
      // TODO catch, already exists
    });
  }

  // ==================================================================================================================================
  // REST Methods
  // ==================================================================================================================================

  public getAllInfos() {

    const reposPromise = this.getAllRepos();
    reposPromise.then((res) => {
      this.setRepoList(res);
      const namePromise = this.gitGetAllRepos();
      namePromise.then((answ) => {
        this.setRepoNameList(answ);
        this.mergeReposToOne();
      }).catch(() => {
        this.mergeReposToOne();
      });
    });

  }

  public gitGetAllRepos() {
    return this.gitService.getRepos();
  }

  public setRepoNameList(repoNameList: string[]) {
    this.repoNameList = repoNameList;
  }

  public getAllRepos() {
    return this.repoService.getAll();
  }

  public setRepoList(repoList: Repository[]) {
    this.repoList = repoList;
  }

  private mergeReposToOne() {
    this.combinedList = [];

    if (this.repoNameList) {
      this.repoNameList.forEach(listName => {
        if (this.repoList.find(repo => repo.name === listName)) {
          this.combinedList.push({
            repo: this.repoList.find(repo => repo.name === listName),
            name: listName,
          });
        } else {
          this.combinedList.push({
            repo: null,
            name: listName,
          });
        }
      });
    }

    if (this.repoList) {
      this.repoList.forEach(listEntry => {
        if (!this.combinedList.find(repo => repo.name === listEntry.name)) {
          this.combinedList.push({
            repo: listEntry,
            name: null,
          });
        }
      });
    }

  }

  // ==================================================================================================================================

  public gitClone() {
    if (this.gitURL === '') {
      this.openSnack('Keine Url angegeben');
      return;
    }

    this.credentialModal.open(undefined, this.gitURL);
  }

  public cloneSuccessful(name: string) {
    this.openSnack('Erfolgreich geklont');
  }

  public postClonedRepo(entry) {
    if (!this.gitURL) {
      this.openSnack('Please enter the gitUrl in the textfield and hit "Select again"');
      return;
    }
    this.cloneSuccessful(entry.name);
  }

  // ==================================================================================================================================
  // Helper Methods
  // ==================================================================================================================================

  public openSnack(text) {
    this.gitURL = '';
    this.getAllInfos();
    this.snackBar.open(text, 'ok');
  }

  public addDynamicStyle() {
    if (!this.user) return '';
    return this.LOGGED_OUT;
  }

  // ==================================================================================================================================
  // Secret Methods
  // ==================================================================================================================================

  public fetchLoginData() {
    const promise = this.userService.getMainUser();
    promise.then((res) => this.useRealUser(res));
    promise.catch((res) => this.useKnownUser());
  }

  public useRealUser(realUser) {
    this.loginData.username = realUser.username;
    this.loginData.password = realUser.password;
    this.login();
  }

  public useKnownUser() {
    this.loginData.username = 'Absolem';
    this.loginData.password = 'Absolem';
    this.login();
  }
}
