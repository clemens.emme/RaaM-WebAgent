import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {RouterModule} from '@angular/router';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule, MatDialogModule,
  MatDividerModule,
  MatGridListModule, MatInputModule,
  MatListModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatDividerModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
    RouterModule.forChild([
      {path: '', component: HomeComponent},
    ]),
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
