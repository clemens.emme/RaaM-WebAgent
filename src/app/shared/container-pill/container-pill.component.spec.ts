import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContainerPillComponent } from './container-pill.component';

describe('ContainerPillComponent', () => {
  let component: ContainerPillComponent;
  let fixture: ComponentFixture<ContainerPillComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContainerPillComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContainerPillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
