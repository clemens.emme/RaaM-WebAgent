import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'containerPill',
  templateUrl: './container-pill.component.html',
  styleUrls: ['./container-pill.component.css']
})
export class ContainerPillComponent implements OnInit {

  @Input() param: any;
  @Input() isInput: boolean = false;

  @Input() prefixSize: number = 50;
  @Input() suffixSize: number = 50;

  @Input() value: any;
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  ngOnInit() {
  }

}
