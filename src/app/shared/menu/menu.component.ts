import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Person} from '../../api/model/person';
import {environment} from '../../../environments/environment';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {UserService} from '../../api/services/user/user.service';
import {RepositoryService} from '../../api/services/repository/repository.service';

@Component({
  selector: 'raamMenu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit, OnDestroy {

  public test = environment.test;

  @Input() actions;
  @Input() user;
  @Input() displayRepo;
  @Input() displayUser;
  @Input() activeLead: boolean;

  constructor(public router: Router,
              public menuBarService: MenuBarService,
              public repoService: RepositoryService,
              public userService: UserService) {
  }

  ngOnInit() {
    this.registerListeners();
  }

  ngOnDestroy() {
    this.unregisterListeners();
  }

  // ==================================================================================================================================
  // Listeners
  // ==================================================================================================================================

  public unregisterListeners() {
    // TODO Unregister
  }

  public registerListeners() {
    this.userService.registerListener('Menu', () => this.getUser());
    this.repoService.registerListener('Menu', () => this.getRepo());
    this.menuBarService.registerListener('Menu', () => this.getRoutingActions());
  }

  public getUser() {
    this.user = this.userService.getUser();
  }

  public getRepo() {
    this.displayRepo = this.repoService.getRepo();
  }

  public getRoutingActions() {
    this.actions = this.menuBarService.getRoutingActions();
    this.displayUser = this.menuBarService.getDisplayUser();
    this.displayRepo = this.menuBarService.getDisplayRepo();
    this.testForActiveLead();
  }

  // ==================================================================================================================================
  // Routing
  // ==================================================================================================================================

  public getRouteTo(route) {
    return () => this.routeTo(route);
  }

  public routeTo(route) {
    this.router.navigateByUrl(route);
  }

  public logout() {
    this.userService.logout();
    this.router.navigateByUrl('/home');
  }

  // ==================================================================================================================================
  // Helper
  // ==================================================================================================================================

  private testForActiveLead() {
    this.activeLead = true;
    for (const action of this.actions) {
      if (action.current) this.activeLead = false;
    }
  }
}
