import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Task} from '../../api/model/task';

@Component({
  selector: 'issueCard',
  templateUrl: './issue-card.component.html',
  styleUrls: ['./issue-card.component.css'],
})
export class IssueCardComponent implements OnInit {
  public readonly CLOSED = 'closed';
  public readonly OPEN = 'open';

  public isClosed = this.CLOSED;

  @Input() task: Task;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  routeDetail() {
    if(!this.task._id) return;
    this.router.navigateByUrl('repository/taskList/' + this.task._id);
  }

  changeView() {
    if (this.isClosed === this.CLOSED) {
      this.isClosed = this.OPEN;
    } else {
      this.isClosed = this.CLOSED;
    }
  }


  // ==================================================================================================================================
  //
  // Task Data Calculations
  //
  // ==================================================================================================================================

  // creates readable time out of workLogs
  createReadableTime() {
    if(!this.task.workLogs) return '-';
    const timeContainer = {days: 0, hours: 0, minutes: 0};

    // Sum up all values from workLogs-Entries
    if (this.task.workLogs) {
      for (const work of this.task.workLogs) {
        timeContainer.hours += work['hoursSpend'];
        timeContainer.minutes += work['minutesSpend'];
      }
    }

    return this.createReadableTimeFrom(timeContainer);
  }

  // creates readable time out if timeContainer given
  createReadableTimeFrom(timeContainer) {
    let answer = '';

    // calculate the carrier
    timeContainer.hours += Math.floor(timeContainer.minutes / Task.MINUTE); // rounding
    timeContainer.minutes = timeContainer.minutes % Task.MINUTE;

    timeContainer.days += Math.floor(timeContainer.hours / Task.HOURS); // rounding
    timeContainer.hours = timeContainer.hours % Task.HOURS;

    if (timeContainer.days > 0) answer += timeContainer.days + 'd ';
    if (timeContainer.hours > 0) answer += timeContainer.hours + 'h ';
    if (timeContainer.minutes > 0) answer += timeContainer.minutes + 'm';

    return answer;
  }
}
