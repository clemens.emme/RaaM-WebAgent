import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'raamDropDown',
  templateUrl: './raam-drop-down.component.html',
  styleUrls: ['./raam-drop-down.component.css'],
  providers: [NgbDropdownConfig],
})
export class RaamDropDownComponent implements OnInit, OnChanges {
  protected readonly UNFOCUSED = 'unfocused';
  protected readonly FOCUSED = 'focused';

  public readonly PLACEHOLDER_DEFUALT = '-';
  public readonly TEXT_REMOVE = 'removeText';

  public missingInput;

  @ViewChild('fix') private fix;
  @ViewChild('myDrop') private myDrop;

  @Input() title = 'Title here';
  @Input() value = '';
  @Input() displayContent: string;
  @Input() currentLable = false;
  @Input() list: any[] = [];
  @Input() display: string;
  @Input() placeholder: string = this.PLACEHOLDER_DEFUALT;

  @Output() change: EventEmitter<any> = new EventEmitter<any>();

  public conjointList = [];
  public displayList = [];
  public serach;
  public focus = this.FOCUSED;
  public isPrimitive;

  constructor() {
  }

  ngOnInit() {
    this.conjoinLists();
    if (this.value) {
      this.focus = this.UNFOCUSED;
    } else {
      this.focus = this.FOCUSED;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.displayList) {
      if (!!changes.list || !!changes.utilList) {
        this.conjoinLists();
      }
    }
  }

  getValueOf(value) {
    if (!value) return this.placeholder;
    if (this.isPrimitive) {
      return value;
    } else {
      const splitList = this.displayContent.split('.');
      if (splitList.length === 1) {
        return value[this.displayContent];
      } else {
        let actualValue = value;
        for (const splitValue of splitList) {
          actualValue = actualValue[splitValue];
        }
        return actualValue;
      }
    }
  }

  changeFocus(focus) {
    if (focus) {
      this.focus = this.FOCUSED;
    } else {
      this.focus = this.UNFOCUSED;
    }
  }

  conjoinLists() {
    if (this.list[0] instanceof Object) {
      this.isPrimitive = false;
    } else {
      this.isPrimitive = true;
    }


    this.conjoinPrimitivLists(this.list);
    this.displayList = this.conjointList;

    this.sortList();
  }

  conjoinPrimitivLists(list) {
    for (const entry of list) {
      if (!entry) continue;
      if (this.conjointList.indexOf(entry) < 0) {
        this.conjointList.push(entry);
      }
    }
  }

  sortList() {
    this.conjointList.sort(this.primitiveSorter());
  }

  primitiveSorter() {
    return function (a, b) {
      if (a < b) return -1;
      if (a > b) return 1;
      return 0;
    };
  }


  log(stuff) {
    if (!stuff) return;
    if (stuff === this.TEXT_REMOVE) stuff = '';
    this.value = stuff;
    this.change.emit(this.value);
  }


  tryList(event) {
    if (event.key !== 'Enter') return;
    if (this.displayList.length === 1) {
      this.myDrop.close();
      this.serach = '';
      this.log(this.displayList[0]);
    }
  }

  filter() {
    if (!this.serach) {
      this.displayList = this.conjointList;
      return;
    }

    if (this.isPrimitive) {
      this.displayList = this.conjointList.filter(this.stringValueContainedFilter(this.serach));
    } else {
      this.displayList = this.conjointList.filter(this.objectContainesValueFilter(this.serach, this.displayContent));
    }


    if (this.displayList.length === 0) {
      this.log('');
    }
  }

  stringValueContainedFilter(value) {
    return function (element) {

      if (value.split(' ').length > 1) {
        let splitSearch = true;
        const list = value.split(' ');
        for (const subValue of list) {
          if (!subValue) continue;
          splitSearch = splitSearch && (element.toLowerCase().indexOf(subValue.toLowerCase()) >= 0);
        }
        return splitSearch;
      }

      return (element.toLowerCase().indexOf(value.toLowerCase()) >= 0);
    };
  }

  objectContainesValueFilter(value, valueParam) {
    return function (element) {
      const fullName = element[valueParam];

      if (value.split(' ').length > 1) {
        let splitSearch = true;
        const list = value.split(' ');
        for (const subValue of list) {
          if (!subValue) continue;
          splitSearch = splitSearch && (fullName.toLowerCase().indexOf(subValue.toLowerCase()) >= 0);
        }
        return splitSearch;
      }

      return (fullName.toLowerCase().indexOf(value.toLowerCase()) >= 0);
    };
  }

}
