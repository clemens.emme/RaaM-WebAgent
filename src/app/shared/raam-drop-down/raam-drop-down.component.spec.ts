import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamDropDownComponent } from './raam-drop-down.component';

describe('RaamDropDownComponent', () => {
  let component: RaamDropDownComponent;
  let fixture: ComponentFixture<RaamDropDownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamDropDownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamDropDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
