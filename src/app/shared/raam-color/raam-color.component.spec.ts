import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamColorComponent } from './raam-color.component';

describe('RaamColorComponent', () => {
  let component: RaamColorComponent;
  let fixture: ComponentFixture<RaamColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamColorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
