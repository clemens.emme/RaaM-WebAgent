import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'raamColor',
  templateUrl: './raam-color.component.html',
  styleUrls: ['./raam-color.component.css']
})
export class RaamColorComponent implements OnInit {

  @Input() color: string;

  constructor() {
  }

  ngOnInit() {
  }

}
