import {Component, Input, OnInit} from '@angular/core';
import {Person} from '../../api/model/person';

@Component({
  selector: 'userCard',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
  public readonly FLIPPED = 'flipped';
  public readonly veryUnpersonalWords =
    'This is basic user profile with image, title, detail and button.';

  @Input() user: Person;
  public flipToggle: string;

  constructor() {
  }

  ngOnInit() {
  }

  toggleFlip() {
    if (this.flipToggle) {
      this.flipToggle = '';
    }
    else {
      this.flipToggle = this.FLIPPED;
    }
  }
}
