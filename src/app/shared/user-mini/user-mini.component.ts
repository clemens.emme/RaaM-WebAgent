import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'userMini',
  templateUrl: './user-mini.component.html',
  styleUrls: ['./user-mini.component.css']
})
export class UserMiniComponent implements OnInit {

  @Input() user;

  constructor() {
  }

  ngOnInit() {
  }

}
