import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCredentialsModalComponent } from './new-credentials-modal.component';

describe('NewCredentialsModalComponent', () => {
  let component: NewCredentialsModalComponent;
  let fixture: ComponentFixture<NewCredentialsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCredentialsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCredentialsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
