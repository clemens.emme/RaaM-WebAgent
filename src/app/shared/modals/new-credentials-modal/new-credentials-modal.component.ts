import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../../api/services/user/user.service';
import {Repository} from '../../../api/model/repository';
import {Person} from '../../../api/model/person';
import {GitService} from '../../../api/services/git/git-service.service';
import {RepositoryService} from '../../../api/services/repository/repository.service';

@Component({
  selector: 'credentialsModal',
  templateUrl: './new-credentials-modal.component.html',
  styleUrls: ['./new-credentials-modal.component.css']
})
export class NewCredentialsModalComponent implements OnInit {
  @ViewChild('newCredentials') private newCredentials;

  public modalRef;
  public repo: Repository;
  public user: Person;
  public url: string;

  public newCreds: any;
  public chosenCred: any;
  public username: string;
  public password: string;
  public observer: boolean;
  public sameAsUser: boolean;

  @Output() finished: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private gitService: GitService,
    private repoService: RepositoryService,
    private userService: UserService) {
  }

  ngOnInit() {
  }

  public open(repo?: any, url?: string) {
    this.resetValues();

    this.user = this.userService.getUser();
    this.repo = repo;
    this.url = url;

    this.modalRef = this.modalService.open(this.newCredentials, {size: 'lg', backdrop: 'static', centered: true, keyboard: false});
    this.modalRef.result.then(() => this.processSave(), () => this.resetValues());
  }

  protected processSave() {
    // This is imported, the upcoming clone would fail otherwise
    this.addCredentialToUser();

    if (this.url) {
      const promise = this.gitService.cloneOneRepoByContextAndUrlAndCredentials(this.url, this.newCreds.username, this.newCreds.password);
      promise.then(name => this.gitCloneCallback(name));
    } else {
      this.putCredentialsAndUser();
    }
  }

  protected resetValues() {
    this.url = undefined;
    this.repo = undefined;
    this.user = undefined;
    this.username = undefined;
    this.password = undefined;
    this.observer = undefined;
    this.newCreds = undefined;
    this.chosenCred = undefined;
  }

  // ==================================================================================================================================
  // PromiseCallbacks
  // ==================================================================================================================================

  public gitCloneCallback(name) {
    const newRepo = new Repository();
    newRepo.url = this.url;
    newRepo.name = name;

    const promise = this.repoService.postOne(newRepo);
    promise.then(repo => this.dbPostCallback(repo));
  }

  public dbPostCallback(repo) {
    this.repo = repo;
    this.putCredentialsAndUser();
  }

  public putCredentialsAndUser() {
    this.addCredentialToUser(true);
    const promise = this.userService.putOwn(this.user);
    promise.then(() => this.finishPromiseChain());
  }

  public finishPromiseChain() {
    this.resetValues();
    this.finished.emit();
  }

  // ==================================================================================================================================
  // Logic
  // ==================================================================================================================================

  protected addCredentialToUser(add?) {
    this.newCreds = {
      repository: this.repo,
      username: undefined,
      password: undefined,
    };

    if (this.observer) {
      this.newCreds.username = '';
      this.newCreds.password = '';
    } else if (this.sameAsUser) {
      this.newCreds.username = this.user.username;
      this.newCreds.password = this.user.password;
    } else if (this.chosenCred) {
      this.newCreds.username = this.chosenCred.username;
      this.newCreds.password = this.chosenCred.password;
    } else {
      this.newCreds.username = this.username;
      this.newCreds.password = this.password;
    }

    if (add) {
      this.user.credentials.push(this.newCreds);
    }
  }

  protected canAdd(): boolean {
    return !!(this.username && this.password) || this.observer || this.sameAsUser || this.chosenCred;
  }

  protected isObserver(input: boolean) {
    this.observer = input;
    if (input) {
      this.username = '';
      this.password = '';
      this.chosenCred = undefined;
      this.sameAsUser = !input;
    }
  }

  protected isSameAsUser(input: boolean) {
    this.sameAsUser = input;
    if (input) {
      this.username = '';
      this.password = '';
      this.chosenCred = undefined;
      this.observer = !input;
    }
  }

  protected useExisting(input: any) {
    this.chosenCred = input;
    this.username = '';
    this.password = '';
    this.sameAsUser = undefined;
    this.observer = undefined;
  }

  protected useUsername(input: string) {
    this.username = input;
    this.chosenCred = undefined;
    this.sameAsUser = undefined;
    this.observer = undefined;
  }

  protected usePassword(input: string) {
    this.password = input;
    this.chosenCred = undefined;
    this.sameAsUser = undefined;
    this.observer = undefined;
  }
}
