import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {GitService} from '../../../api/services/git/git-service.service';
import {RepositoryService} from '../../../api/services/repository/repository.service';
import {Task} from '../../../api/model/task';
import {Repository} from '../../../api/model/repository';
import {TaskService} from '../../../api/services/task/task.service';
import {MenuBarService} from '../../../api/services/actionBar/menu-bar.service';
import {ContainerService} from '../../../api/services/container/container.service';
import {ContainerModel} from '../../../api/model/containerModel';

@Component({
  selector: 'newTaskModal',
  templateUrl: './new-task-modal.component.html',
  styleUrls: ['./new-task-modal.component.css']
})
export class NewTaskModalComponent implements OnInit {
  @ViewChild('newTask') private newTask;
  public readonly NEW_TASK_MODAL_SECRET_ACTIONS = [
    {text: 'reset All', call: () => this.closeModal()},
  ];

  protected modalRef;
  @Output() nextTaskCallback: EventEmitter<any> = new EventEmitter();


  // Boolean Success Indicator
  protected fileGenWorked;
  protected databaseWorked;
  protected gitWorked;

  // Request Data
  protected taskToCreate: Task;
  protected parameters: any;

  // Possible Responded Objects
  protected blob: Blob;
  protected task: Task;
  protected fileName: string;
  protected repository: Repository;


  constructor(private router: Router,
              private modalService: NgbModal,
              private menuService: MenuBarService,
              private gitService: GitService,
              private taskService: TaskService,
              private repoService: RepositoryService,
              private containerService: ContainerService) {
  }

  ngOnInit() {
  }

  // ==================================================================================================================================
  // Execution Methods
  // ==================================================================================================================================

  public open(task: Task, params: any) {
    this.menuService.setSecretActions(this.NEW_TASK_MODAL_SECRET_ACTIONS);

    this.repository = this.repoService.getRepo();
    this.taskToCreate = task;
    this.parameters = params;

    this.modalRef = this.modalService.open(this.newTask, {backdrop: 'static', centered: true, keyboard: false});
    this.modalRef.result.then((result) => this.closeModal(), (reason) => {
    });

    this.startCallChain();
  }

  // TODO make same calls again but fo real
  protected retry() {
    if (!this.databaseWorked) {
      const database = this.buildPromiseDummy(2000);
      this.databaseWorked = undefined;
      this.setTestDatabaseCallback(database);
    }
    if (!this.gitWorked) {
      const git = this.buildPromiseDummy(2500);
      this.gitWorked = undefined;
      this.setTestGitCallback(git);
    }
  }

  protected delete() {
    this.closeModal();
    // TODO delete on both sides
    this.modalRef.close();
  }

  // ==================================================================================================================================
  // Data Persisting Callchain
  // ==================================================================================================================================

  private startCallChain() {
    if (this.parameters && this.taskToCreate.type.canLinkFile) {
      this.generateFile();
    } else {
      this.setFileNotNeeded();
      this.persistTask();
    }
  }

  private generateFile() {
    this.setCallbacks(
      this.postFileGeneration(this.parameters, this.taskToCreate.container),
      this.fileGenPositive.bind(this),
      this.fileGenNegative.bind(this));
  }

  private uploadFile() {
    this.setCallbacks(
      this.postFileToGit(this.blob, this.fileName),
      this.fileUpPositive.bind(this),
      this.fileUpNegative.bind(this));
  }

  private persistBoth() {
    this.persistFileInCommit();
    this.persistTask();
  }

  private persistFileInCommit() {
    this.setCallbacks(
      this.commit(this.taskToCreate),
      this.gitPositive.bind(this),
      this.gitNegative.bind(this));
  }

  private persistTask() {
    this.setCallbacks(
      this.postOneTask(this.taskToCreate),
      this.databasePositive.bind(this),
      this.databaseNegative.bind(this));
  }

  private setCallbacks(promise: Promise<any>, positiveResponse: any, negativeResponse: any) {
    promise.then((res) => positiveResponse(res), (reason) => negativeResponse(reason));
  }

  // ==================================================================================================================================
  // REST Calls
  // ==================================================================================================================================

  private postFileGeneration(param: any, container: ContainerModel): Promise<any> {
    return this.containerService.postOneContainerParameters(param, container);
  }

  private postFileToGit(blob: Blob, fileName: string): Promise<any> {
    return this.gitService.uploadBlob(blob, fileName);
  }

  private postOneTask(task: Task): Promise<any> {
    return this.taskService.postOne(task);
  }

  private putRepo(repo: Repository): Promise<any> {
    return this.repoService.putOne(repo);
  }

  private getRepo(repo: Repository): Promise<any> {
    return this.repoService.getOneByName(repo.name);
  }

  private commit(task: Task): Promise<any> {
    return this.gitService.postOneCommitByContext(task);
  }

  // ==================================================================================================================================
  // Response Status Management
  // ==================================================================================================================================

  protected setFileNotNeeded() {
    this.fileGenWorked = true;
    this.gitWorked = true;
  }

  // get file back to commit/push
  protected fileGenPositive(blob) {
    this.blob = blob;
    this.fileName = this.parameters.name + '.' + this.taskToCreate.container.format;
    this.taskToCreate.filePath += this.fileName;

    this.uploadFile();
  }

  protected fileGenNegative(reason) {
    this.fileGenWorked = false;
  }

  // get file back to commit/push
  protected fileUpPositive(blob) {
    this.fileGenWorked = true;
    this.persistBoth();
  }

  protected fileUpNegative(reason) {
    this.fileGenWorked = false;
  }

  // gets Task Back
  protected databasePositive(res) {
    this.task = res;
    this.databaseWorked = true;
  }

  protected databaseNegative(reason) {
    // TODO Display reason
    this.databaseWorked = false;
  }


  // TODO what will i give back here?
  // TODO probably the commit id to be able to revert the commits
  protected gitPositive() {
    this.gitWorked = true;
  }

  protected gitNegative(reason) {
    // TODO Display reason
    this.gitWorked = false;
  }

  protected closeModal() {
    if (this.task) this.repository.tasks.push(this.task);
    this.setCallbacks(
      this.putRepo(this.repository),
      this.refreshRepo.bind(this),
      this.resetValues.bind(this)
    );
  }

  protected refreshRepo() {
    this.repository.tasks.push(this.task);
    this.setCallbacks(
      this.getRepo(this.repository),
      this.resetValues.bind(this),
      this.resetValues.bind(this)
    );
  }

  protected resetValues() {
    this.fileGenWorked = undefined;
    this.databaseWorked = undefined;
    this.gitWorked = undefined;
    this.taskToCreate = undefined;
    this.parameters = undefined;
    this.blob = undefined;
    this.task = undefined;
    this.repository = undefined;
  }

  // ==================================================================================================================================
  // Request Callback Management
  // ==================================================================================================================================

  protected setTestCallbacks(database, git) {
    this.setTestDatabaseCallback(database);
    this.setTestGitCallback(git);
  }

  private setTestDatabaseCallback(database) {
    this.setCallbacks(database,
      this.databasePositive.bind(this),
      this.databaseNegative.bind(this)
    );
  }

  private setTestGitCallback(git) {
    this.setCallbacks(git,
      this.gitPositive.bind(this),
      this.gitNegative.bind(this)
    );
  }

  private buildPromiseDummy(timeout: number) {
    return new Promise((resolve, reject) => setTimeout(() => resolve(), timeout));
  }

  // ==================================================================================================================================
  // Routing Options
  // ==================================================================================================================================


  protected routeToList() {
    this.modalRef.close();
    this.router.navigateByUrl('repository/taskList');
  }

  protected nextTask() {
    this.nextTaskCallback.emit();
    this.modalRef.close();
  }
}
