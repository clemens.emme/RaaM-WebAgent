import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultRolesModalComponent } from './default-roles-modal.component';

describe('DefaultRolesModalComponent', () => {
  let component: DefaultRolesModalComponent;
  let fixture: ComponentFixture<DefaultRolesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultRolesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultRolesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
