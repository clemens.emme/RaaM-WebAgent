import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RoleService} from '../../../api/services/role/role.service';
import {Role} from '../../../api/model/role';

@Component({
  selector: 'defaultRolesModal',
  templateUrl: './default-roles-modal.component.html',
  styleUrls: ['./default-roles-modal.component.css']
})
export class DefaultRolesModalComponent implements OnInit {
  @ViewChild('defaultRoles') private defaultRoles;

  protected modalRef;
  protected createdCount = 0;
  protected roles: Role[] = [
    new Role().setToArtistDefault(),
    new Role().setToDeveloperDefault(),
  ];

  @Output() finished: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
              private roleService: RoleService) {
  }

  ngOnInit() {
  }

  // =======================================================================================================================

  public open() {
    this.resetValues();

    this.modalRef = this.modalService.open(this.defaultRoles, {backdrop: 'static', centered: true});
    this.modalRef.result.then(() => {
      // Close
      this.resetValues();
      this.finished.emit();
    }, () => {
      // Dismiss
      this.resetValues();
    });
  }

  // =======================================================================================================================

  protected addDefaultRolesToDatabase() {
    for (const role of this.roles) {
      this.roleService.postOne(role).then(res => {
        this.createdCount++;
      });
    }
  }

  public resetValues() {
    this.createdCount = 0;
  }
}
