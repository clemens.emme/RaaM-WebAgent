import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultTaskTypesModalComponent } from './default-task-types-modal.component';

describe('DefaultTaskTypesModalComponent', () => {
  let component: DefaultTaskTypesModalComponent;
  let fixture: ComponentFixture<DefaultTaskTypesModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultTaskTypesModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultTaskTypesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
