import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TaskTypeService} from '../../../api/services/task-type/task-type.service';
import {TaskType} from '../../../api/model/task-type';

@Component({
  selector: 'defaultTaskTypesModal',
  templateUrl: './default-task-types-modal.component.html',
  styleUrls: ['./default-task-types-modal.component.css']
})
export class DefaultTaskTypesModalComponent implements OnInit {
  @ViewChild('defaultTaskTypes') private defaultTaskTypes;

  protected modalRef;
  protected createdCount = 0;
  protected taskTypes: TaskType[] = [
    new TaskType().setToEpicDefault(),
    new TaskType().setToStoryDefault(),
    new TaskType().setToSubTaskDefault(),
    new TaskType().setToBugDefault(),
    new TaskType().setToAssetDefault(),
  ];

  @Output() finished: EventEmitter<any> = new EventEmitter();

  constructor(private modalService: NgbModal,
              private taskTypeService: TaskTypeService) {
  }

  ngOnInit() {
  }

  // =======================================================================================================================

  public open() {
    this.resetValues();

    this.modalRef = this.modalService.open(this.defaultTaskTypes, {backdrop: 'static', centered: true});
    this.modalRef.result.then(() => {
      // Close
      this.resetValues();
      this.finished.emit();
    }, () => {
      // Dismiss
      this.resetValues();
    });
  }

  // =======================================================================================================================

  protected addDefaultTaskTypesToDatabase() {
    for (const taskType of this.taskTypes) {
      this.taskTypeService.postOne(taskType).then(res => {
        this.createdCount++;
      });
    }
  }

  public resetValues() {
    this.createdCount = 0;
  }
}
