import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ContainerModel} from '../../../api/model/containerModel';
import {ContainerService} from '../../../api/services/container/container.service';
import {RoleService} from '../../../api/services/role/role.service';
import {Role} from '../../../api/model/role';
import {MenuBarService} from '../../../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'containerModal',
  templateUrl: './new-container-modal.component.html',
  styleUrls: ['./new-container-modal.component.css']
})
export class NewContainerModalComponent implements OnInit {
  @ViewChild('newContainer') private newContainer;
  public readonly NEW_CONTAINER_MODAL_SECRET_ACTIONS = [
    {text: 'test Config', call: () => this.setTestConfig()},
    {text: 'reset All', call: () => this.resetValues()},
  ];
  public readonly containerTestMethods = [
    this.containerHasOwnName.bind(this),
    this.containerHasFormat.bind(this),
    this.containerHasParameters.bind(this),
    this.containerHasNameParameter.bind(this),
  ];


  private readonly testConfig = {
    name: 'New Name',
    format: '.gif',
    description: 'This is a local test object. Its not meant to be added to the system',
    parameters: [
      {name: 'name'},
      {name: 'width'},
      {name: 'height'},
      {name: 'color'},
      {name: 'colorSchema'},
    ],
  };

  protected modalRef;
  protected url: string;
  protected isOk: boolean;
  protected thirtyReasonsWhy: object[];
  protected container: ContainerModel;
  protected roles: Role[];
  protected role: Role;

  @Output() finished: EventEmitter<any> = new EventEmitter();

  constructor(
    private modalService: NgbModal,
    private roleService: RoleService,
    private menuBarService: MenuBarService,
    private containerService: ContainerService) {
  }

  ngOnInit() {
    this.menuBarService.setSecretActions(this.NEW_CONTAINER_MODAL_SECRET_ACTIONS);
  }

  // =======================================================================================================================

  public open() {
    this.resetValues();

    this.modalRef = this.modalService.open(this.newContainer, {size: 'lg', backdrop: 'static', centered: true, keyboard: false});
    this.modalRef.result.then(() => this.processSave(), () => this.resetValues());
  }

  protected processSave() {
    this.container.url = this.url;
    this.containerService.postOne(this.container).then(res => {
      this.resetValues();
      this.finished.emit();
    });
  }

  protected resetValues() {
    this.url = '';
    this.isOk = undefined;
    this.container = undefined;
    this.thirtyReasonsWhy = [];
    this.roles = undefined;
    this.role = undefined;
  }

  protected changeContainerColor(value) {
    if (value instanceof Object) {
      this.role = value;
      this.container.color = value.color;
    } else {
      this.container.color = value;
    }
  }

  private setTestConfig() {
    this.processResponse(this.testConfig);
  }

  // =======================================================================================================================

  protected getAllRoles() {
    this.roleService.getAll().then(res => {
      this.roles = res;
    });
  }

  protected testContainer() {
    this.containerService.getOneContainerConfig(this.url)
      .then(res => this.processResponse(res))
      .catch(res => this.errorCatching(res));
  }

  private processResponse(res) {
    this.container = res;
    this.isContainerValid();
  }

  private errorCatching(res) {
    if (res.status === 404) { // status: 404 Not Found
      this.containerNotFound();
    }
    else if (res.status === 0) { // status: 0 Connection Refused
      this.containerForbidden();
    }
    else {
      console.log(res);
    }
  }

  // =======================================================================================================================

  private isContainerValid() {
    this.containerTestMethods.forEach(method => method());

    // If there is nothing to complain, the user can add the container
    if (this.thirtyReasonsWhy.length === 0) {
      this.isOk = true;
      this.getAllRoles();
    }
  }

  private addToReasons(short: string, explanation: string) {
    this.thirtyReasonsWhy.push({
      short: short,
      explanation: explanation,
    });
  }

  // =======================================================================================================================
  private containerNotFound() {
    this.addToReasons(
      '404 not Found',
      '404 Container not Found, the url might have a typo'
    );
  }

  private containerForbidden() {
    this.addToReasons(
      'Connection Forbidden',
      'The Request is responded with a status: 0, maybe check the cors'
    );
  }

  public containerHasOwnName() {
    if (!this.container.name) {
      this.addToReasons(
        'No Name',
        'The container needs to return a selfindicating name'
      );
    }
  }

  public containerHasFormat() {
    if (!this.container.format) {
      this.addToReasons(
        'No Format',
        'The container needs to return a format that it can create'
      );
    }
  }

  public containerHasParameters() {
    if (!this.container.parameters) {
      this.addToReasons(
        'No Parameters',
        'The container needs to return a list of parameters it can process when creating a new file.'
      );
    }
  }

  public containerHasNameParameter() {
    if (!this.container.parameters) return;
    if (this.container.parameters.findIndex(param => param['name'] === 'name') < 0) {
      this.addToReasons(
        'No Name Parameter',
        'The container needs to return one parameter that has the name: "name" that represents the later generated file name'
      );
    }
  }
}
