import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewContainerModalComponent } from './new-container-modal.component';

describe('NewContainerModalComponent', () => {
  let component: NewContainerModalComponent;
  let fixture: ComponentFixture<NewContainerModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewContainerModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewContainerModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
