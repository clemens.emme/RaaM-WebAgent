import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamInputComponent } from './raam-input.component';

describe('RaamInputComponent', () => {
  let component: RaamInputComponent;
  let fixture: ComponentFixture<RaamInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
