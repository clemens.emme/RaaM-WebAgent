import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'raamInput',
  templateUrl: './raam-input.component.html',
  styleUrls: ['./raam-input.component.css'],
  providers: [NgbDropdownConfig],
})
export class RaamInputComponent implements OnInit {
  protected readonly UNFOCUSED = 'unfocused';
  protected readonly FOCUSED = 'focused';

  @Input() title = 'Title';
  @Input() type = 'text';
  @Input() value;
  @Input() placeholder = '';
  @Input() areaText = false;
  @Output() valueChange = new EventEmitter();

  public focus = this.FOCUSED;

  constructor() {
  }

  ngOnInit() {
    if (this.value) {
      this.focus = this.UNFOCUSED;
    } else {
      this.focus = this.FOCUSED;
    }
  }

  onChange(e) {
    this.valueChange.emit(e.target.value);
  }

  changeFocus(focus) {
    if (focus) {
      this.focus = this.FOCUSED;
    } else {
      this.focus = this.UNFOCUSED;
    }
  }
}
