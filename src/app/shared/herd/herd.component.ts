import {Component, HostListener, OnInit} from '@angular/core';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'herd',
  templateUrl: './herd.component.html',
  styleUrls: ['./herd.component.css']
})
export class HerdComponent implements OnInit {
  public defaultCommands = [
    {text: 'list', call: () => this.listCommands()},
    {text: 'start riot', call: () => this.startRiot()},
    {text: 'stop riot', call: () => this.stopRiot()},
  ];
  public externalCommands = [];
  public commands = [];

  public riot;
  public toggle;
  public command = '';
  public showList = false;

  constructor(private menubarService: MenuBarService) {
  }

  ngOnInit() {
    this.menubarService.registerListener('Herd', () => this.getSecretActions());
  }

  public getSecretActions() {
    this.externalCommands = this.menubarService.getSecretActions();
    this.commands = this.defaultCommands.concat(this.externalCommands);
  }

  // ==================================================================================================================================
  // Key Listeners
  // ==================================================================================================================================

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (!this.toggle) return;
    if (event.key === 'Enter') return;
    if (event.key === 'Backspace') return;
    this.command += event.key;
  }

  @HostListener('document:keyup.Escape', ['$event'])
  onKeyHandler(event: KeyboardEvent) {
    this.toggle = !this.toggle;
  }

  @HostListener('document:keyup.Enter', ['$event'])
  onKeyupHandler(event: KeyboardEvent) {
    if (!this.toggle) return;
    this.searchCommand();
  }

  @HostListener('document:keydown.Backspace', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    if (!this.toggle) return;
    this.command = this.command.slice(0, -1);
  }

  private searchCommand() {
    for (const command of this.commands) {
      if (command.text.toLowerCase() === this.command.toLowerCase()) {
        this.command = '';
        command.call();
        return;
      }
    }
  }

  // ==================================================================================================================================
  // Default Calls Methods
  // ==================================================================================================================================

  public startRiot() {
    this.riot = true;
  }

  public stopRiot() {
    this.riot = false;
  }

  public listCommands() {
    this.showList = !this.showList;
  }
}
