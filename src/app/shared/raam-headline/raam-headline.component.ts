import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'raamHeadline',
  templateUrl: './raam-headline.component.html',
  styleUrls: ['./raam-headline.component.css']
})
export class RaamHeadlineComponent implements OnInit {

  @Input() title = 'Title';

  constructor() { }

  ngOnInit() {
  }

}
