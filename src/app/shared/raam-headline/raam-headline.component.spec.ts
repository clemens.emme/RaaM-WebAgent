import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamHeadlineComponent } from './raam-headline.component';

describe('RaamHeadlineComponent', () => {
  let component: RaamHeadlineComponent;
  let fixture: ComponentFixture<RaamHeadlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamHeadlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamHeadlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
