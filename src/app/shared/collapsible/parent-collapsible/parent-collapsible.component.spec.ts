import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentCollapsibleComponent } from './parent-collapsible.component';

describe('ParentCollapsibleComponent', () => {
  let component: ParentCollapsibleComponent;
  let fixture: ComponentFixture<ParentCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
