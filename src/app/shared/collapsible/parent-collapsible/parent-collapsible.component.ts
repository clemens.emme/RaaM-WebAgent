import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../../api/model/task';
import {RepositoryService} from '../../../api/services/repository/repository.service';

@Component({
  selector: 'parentCollapsible',
  templateUrl: './parent-collapsible.component.html',
  styleUrls: ['./parent-collapsible.component.css']
})
export class ParentCollapsibleComponent implements OnInit {
  public readonly PARENT_TITLE: string = 'Parent Task';
  public readonly PARENT_DISPLAY_CONTENT: string = 'title';

  // ===================================================== Assignment In And Output ===================================================== //

  @Input() parent: Task;
  @Output() outputParent: EventEmitter<Task> = new EventEmitter<Task>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public tasks: Task[];

  constructor(private repoService: RepositoryService) {
  }

  ngOnInit() {
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getAllTasksFromRepo();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllTasksFromRepo() {
    this.tasks = this.repoService.getRepo().tasks;
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected setTask(task: Task) {
    this.parent = task;
    this.outputParent.emit(this.parent);
  }

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public setDefault() {
    this.setTask(this.tasks.find(task => task.title === 'Story Title'));
  }

}
