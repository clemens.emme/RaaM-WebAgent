import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {GitService} from '../../../api/services/git/git-service.service';

@Component({
  selector: 'filePathCollapsible',
  templateUrl: './file-path-collapsible.component.html',
  styleUrls: ['./file-path-collapsible.component.css']
})
export class FilePathCollapsibleComponent implements OnInit, OnChanges {
  public readonly FILE_PATH_TITLE: string = 'File Path';

  // ===================================================== Assignment In And Output ===================================================== //
  @Input() filePath: string;
  @Input() branch: string;
  @Output() outputFilePath: EventEmitter<string> = new EventEmitter<string>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();

  public fileSystem;

  constructor(private gitService: GitService) {
  }

  ngOnInit() {
    this.initDataRequests();
  }

  ngOnChanges() {
    if (!this.isCollapsed) {
      this.getFileSystem();
    }
  }

  private initDataRequests() {
    this.getFileSystem();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getFileSystem() {
    this.gitService.getOneReposFileSystemByContextAndBranch({branch: this.branch}).then(res => {
      this.fileSystem = res;
    });
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

  protected setFilePath(filePath: string) {
    this.filePath = filePath;
    this.outputFilePath.emit(this.filePath);
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public setDefault() {
    this.setFilePath('src/main/resources/de/uniks/se/');
  }
}
