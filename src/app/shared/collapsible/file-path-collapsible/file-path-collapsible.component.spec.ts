import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilePathCollapsibleComponent } from './file-path-collapsible.component';

describe('FilePathCollapsibleComponent', () => {
  let component: FilePathCollapsibleComponent;
  let fixture: ComponentFixture<FilePathCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilePathCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilePathCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
