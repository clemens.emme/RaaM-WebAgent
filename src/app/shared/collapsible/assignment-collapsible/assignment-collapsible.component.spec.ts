import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentCollapsibleComponent } from './assignment-collapsible.component';

describe('AssignmentCollapsibleComponent', () => {
  let component: AssignmentCollapsibleComponent;
  let fixture: ComponentFixture<AssignmentCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
