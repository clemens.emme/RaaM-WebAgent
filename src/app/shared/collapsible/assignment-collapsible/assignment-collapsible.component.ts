import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskType} from '../../../api/model/task-type';
import {TaskTypeService} from '../../../api/services/task-type/task-type.service';

@Component({
  selector: 'assignmentCollapsible',
  templateUrl: './assignment-collapsible.component.html',
  styleUrls: ['./assignment-collapsible.component.css']
})
export class AssignmentCollapsibleComponent implements OnInit {
  public readonly ASSIGNMENT_TITLE: string = 'Assignment';
  public readonly TASK_TYPES_DISPLAY_CONTENT: string = 'name';
  public readonly SCRUM_CARD_SIZE: number = 7;

  // ===================================================== Assignment In And Output ===================================================== //
  @Input() title: string;
  @Input() taskType: TaskType;
  @Input() description: string;
  @Input() estimatedPoints: number;

  @Output() outputTitle: EventEmitter<string> = new EventEmitter<string>();
  @Output() outputTaskType: EventEmitter<TaskType> = new EventEmitter<TaskType>();
  @Output() outputDescription: EventEmitter<string> = new EventEmitter<string>();
  @Output() outputEstimatedPoints: EventEmitter<number> = new EventEmitter<number>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public taskTypes: TaskType[];

  constructor(private taskTypeService: TaskTypeService) {
  }

  ngOnInit() {
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getAllTaskTypes();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllTaskTypes() {
    this.taskTypeService.getAll().then(res => {
        this.taskTypes = res;
      }
    );
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

  protected setTitle(title: string) {
    this.title = title;
    this.outputTitle.emit(this.title);
  }

  protected setTaskType(taskType: TaskType) {
    this.taskType = taskType;
    this.outputTaskType.emit(this.taskType);
  }

  protected setEstimatedPoints(points: number) {
    this.estimatedPoints = points;
    this.outputEstimatedPoints.emit(this.estimatedPoints);
  }

  protected setDescription(description: string) {
    this.description = description;
    this.outputDescription.emit(this.description);
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public setDefault() {
    this.setTitle('Test HEad');
    this.setTaskType(this.taskTypes.find(task => task.name === 'Asset'));
    this.setEstimatedPoints(3);
    this.setDescription('Test I\'m a testi test');
  }
}
