import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {GitService} from '../../../api/services/git/git-service.service';
import {WaitFor} from '../../../utils/wait-for';

@Component({
  selector: 'branchnameCollapsible',
  templateUrl: './branch-name-collapsible.component.html',
  styleUrls: ['./branch-name-collapsible.component.css']
})
export class BranchNameCollapsibleComponent extends WaitFor implements OnInit {
  public readonly BRANCH_NAME_TITLE: string = 'Branch';
  public readonly BRANCH_NAME_DISPLAY_CONTENT: string = 'short';

  // ===================================================== Assignment In And Output ===================================================== //
  @Input() branchName: string;
  @Output() outputBranchName: EventEmitter<string> = new EventEmitter<string>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public branchList: string[];

  constructor(private gitService: GitService) {
    super();
  }

  ngOnInit() {
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getBranchList();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getBranchList() {
    this.gitService.getOneReposBranchListByContext().then(res => {
        // map to just strings
        this.branchList = res.map(branch => branch.short);
      }
    );
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

  protected setBranchName(name: string) {
    this.branchName = name;
    this.outputBranchName.emit(name);
  }


  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================
  public setDefault() {
    this.waitForThenDo(this, 'branchList', () => {
      this.setBranchName(this.branchList.find( branch => branch === 'master'));
    });
  }
}
