import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchNameCollapsibleComponent } from './branch-name-collapsible.component';

describe('BranchNameCollapsibleComponent', () => {
  let component: BranchNameCollapsibleComponent;
  let fixture: ComponentFixture<BranchNameCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchNameCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchNameCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
