import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ContainerService} from '../../../api/services/container/container.service';
import {ContainerModel} from '../../../api/model/containerModel';
import {WaitFor} from '../../../utils/wait-for';

@Component({
  selector: 'containerCollapsible',
  templateUrl: './container-collapsible.component.html',
  styleUrls: ['./container-collapsible.component.css']
})
export class ContainerCollapsibleComponent extends WaitFor implements OnInit {
  public readonly CONTAINER_TITLE: string = 'Asset';
  public readonly CONTAINER_DISPLAY_CONTENT: string = 'format';

  // ===================================================== Container In And Output ===================================================== //

  @Input() parameters: any;
  @Input() container: ContainerModel;
  @Output() outputParameters: EventEmitter<any> = new EventEmitter<any>();
  @Output() outputContainer: EventEmitter<ContainerModel> = new EventEmitter<ContainerModel>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();

  public containerList: ContainerModel[];

  constructor(private containerService: ContainerService) {
    super();
  }

  ngOnInit() {
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getAllContainer();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllContainer() {
    this.containerService.getAll().then(res => this.containerList = res);
  }

  // ==================================================================================================================================
  // Parameter editing methods
  // ==================================================================================================================================

  public getParamValue(param: any) {
    if (!this.parameters) this.parameters = {};
    return this.parameters[param.name];
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }


  protected setContainer(container: ContainerModel) {
    this.container = container;
    this.outputContainer.emit(this.container);
  }

  public setParamValue(param: any, value: any) {
    this.parameters[param.name] = value;
    this.outputParameters.emit(this.parameters);
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public setDefault() {
    this.waitForThenDo(this, 'containerList',
      () => this.setContainer(this.containerList.find(cont => cont.format === 'png'))
    );
  }
}
