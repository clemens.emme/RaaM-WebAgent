import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';

@Component({
  selector: 'defaultCollapsible',
  templateUrl: './default-collapsible.component.html',
  styleUrls: ['./default-collapsible.component.css'],
})
export class DefaultCollapsibleComponent implements OnInit {
  @ViewChild('collapser') collapser: ElementRef;


  @Input() title = 'Collapse';
  @Input() isCollapsed = true;

  @Output() opening: EventEmitter<void> = new EventEmitter<void>();
  @Output() closing: EventEmitter<void> = new EventEmitter<void>();

  @Output() collapseChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit() {
  }

  // ==================================================================================================================================
  // Collapse Methods
  // ==================================================================================================================================

  public changeCollapse() {
    if (this.isCollapsed) {
      this.openCollapse();
    } else {
      this.closeCollapse();
    }
  }

  public openCollapse() {
    this.isCollapsedChange();
    this.opening.emit();
  }

  public closeCollapse() {
    this.isCollapsedChange();
    this.closing.emit();

  }

  private isCollapsedChange() {
    this.isCollapsed = !this.isCollapsed;
    this.collapseChange.emit(this.isCollapsed);
  }
}
