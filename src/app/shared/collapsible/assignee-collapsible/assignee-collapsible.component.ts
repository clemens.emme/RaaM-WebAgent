import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from '../../../api/model/person';
import {UserService} from '../../../api/services/user/user.service';

@Component({
  selector: 'assigneeCollapsible',
  templateUrl: './assignee-collapsible.component.html',
  styleUrls: ['./assignee-collapsible.component.css']
})
export class AssigneeCollapsibleComponent implements OnInit {
  public readonly ASSIGNEE_TITLE: string = 'Assignee';
  public readonly ASSIGNEE_DISPLAY_CONTENT: string = 'username';

  // ===================================================== Assignment In And Output ===================================================== //

  @Input() assignee: Person;
  @Output() outputAssignee: EventEmitter<Person> = new EventEmitter<Person>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public persons: Person[];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getAllTaskTypes();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllTaskTypes() {
    this.userService.getAll().then(res => {
        this.persons = res;
      }
    );
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected setAssignee(assignee: Person) {
    this.assignee = assignee;
    this.outputAssignee.emit(this.assignee);
  }

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public setDefault() {
    this.setAssignee(this.persons.find( person => person.lastName === 'Emme'));
  }

}
