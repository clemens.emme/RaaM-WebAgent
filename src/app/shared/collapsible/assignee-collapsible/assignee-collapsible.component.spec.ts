import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssigneeCollapsibleComponent } from './assignee-collapsible.component';

describe('AssigneeCollapsibleComponent', () => {
  let component: AssigneeCollapsibleComponent;
  let fixture: ComponentFixture<AssigneeCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssigneeCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssigneeCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
