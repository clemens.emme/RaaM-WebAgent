import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildrenCollapsibleComponent } from './children-collapsible.component';

describe('ChildrenCollapsibleComponent', () => {
  let component: ChildrenCollapsibleComponent;
  let fixture: ComponentFixture<ChildrenCollapsibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildrenCollapsibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildrenCollapsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
