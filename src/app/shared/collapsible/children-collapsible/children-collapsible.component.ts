import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../../api/model/task';
import {TaskService} from '../../../api/services/task/task.service';
import {RepositoryService} from '../../../api/services/repository/repository.service';

@Component({
  selector: 'childrenCollapsible',
  templateUrl: './children-collapsible.component.html',
  styleUrls: ['./children-collapsible.component.css']
})
export class ChildrenCollapsibleComponent implements OnInit {
  public readonly CHILDREN_TITLE: string = 'Child Tasks';
  public readonly CHILDREN_DISPLAY_CONTENT: string = 'title';

  // ===================================================== Assignment In And Output ===================================================== //

  @Input() children: Task[];
  @Output() outputChildren: EventEmitter<Task[]> = new EventEmitter<Task[]>();

  // ============================================================ Variables ============================================================= //

  @Input() isCollapsed = true;
  @Output() outputCollapsed: EventEmitter<boolean> = new EventEmitter<boolean>();
  public tasks: Task[];

  constructor(private repoService: RepositoryService) {
  }

  ngOnInit() {
    this.children = [];
    this.initDataRequests();
  }

  private initDataRequests() {
    this.getAllTasksFromRepo();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllTasksFromRepo() {
    this.tasks = this.repoService.getRepo().tasks;
  }

  // ==================================================================================================================================
  // Output methods
  // ==================================================================================================================================

  protected taskSelected(task: Task) {
    const indexChildren = this.children.findIndex(child => child.title === task.title);
    if (indexChildren < 0) {
      this.children.push(task);
    } else {
      this.children.splice(indexChildren, 1);
    }
    this.outputChildren.emit(this.children);

  }

  protected changeCollapse(newCollapse: boolean) {
    this.isCollapsed = newCollapse;
    this.outputCollapsed.emit(this.isCollapsed);
  }

}
