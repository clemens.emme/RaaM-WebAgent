import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FsEntryComponent} from './fs-entry/fs-entry.component';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatDialogModule, MatDividerModule,
  MatGridListModule, MatInputModule,
  MatListModule,
  MatSnackBarModule,
  MatTooltipModule
} from '@angular/material';
import {MenuComponent} from './menu/menu.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {IssueCardComponent} from './issue-card/issue-card.component';
import {RaamInputComponent} from './raam-input/raam-input.component';
import {RaamHeadlineComponent} from './raam-headline/raam-headline.component';
import {RaamDropDownComponent} from './raam-drop-down/raam-drop-down.component';
import {NewTaskModalComponent} from './modals/new-task-modal/new-task-modal.component';
import {RaamCardComponent} from './raam-card/raam-card.component';
import {HerdComponent} from './herd/herd.component';
import {ContainerCardComponent} from './container-card/container-card.component';
import {NewContainerModalComponent} from './modals/new-container-modal/new-container-modal.component';
import {UserCardComponent} from './user-card/user-card.component';
import {UserMiniComponent} from './user-mini/user-mini.component';
import {ScrumCardComponent} from './scrum-card/scrum-card.component';
import {RaamSwitchComponent} from './raam-switch/raam-switch.component';
import {RouterModule} from '@angular/router';
import {DefaultRolesModalComponent} from './modals/default-roles-modal/default-roles-modal.component';
import {RaamColorComponent} from './raam-color/raam-color.component';
import {DefaultCollapsibleComponent} from './collapsible/default-collapseible/default-collapsible.component';
import {DefaultTaskTypesModalComponent} from './modals/default-task-types-modal/default-task-types-modal.component';
import {AssignmentCollapsibleComponent} from './collapsible/assignment-collapsible/assignment-collapsible.component';
import {AssigneeCollapsibleComponent} from './collapsible/assignee-collapsible/assignee-collapsible.component';
import {ParentCollapsibleComponent} from './collapsible/parent-collapsible/parent-collapsible.component';
import {ChildrenCollapsibleComponent} from './collapsible/children-collapsible/children-collapsible.component';
import {FilePathCollapsibleComponent} from './collapsible/file-path-collapsible/file-path-collapsible.component';
import {BranchNameCollapsibleComponent} from './collapsible/branch-name-collapsible/branch-name-collapsible.component';
import {ContainerCollapsibleComponent} from './collapsible/container-collapsible/container-collapsible.component';
import {ContainerPillComponent} from './container-pill/container-pill.component';
import { FileCardComponent } from './file-card/file-card.component';
import { RepoCradComponent } from './repo-crad/repo-crad.component';
import { NewCredentialsModalComponent } from './modals/new-credentials-modal/new-credentials-modal.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    FlexLayoutModule,
    RouterModule,

    MatButtonModule,
    MatInputModule,
    MatListModule,
    MatDividerModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatGridListModule,
    MatCardModule,
    MatDialogModule,
  ],
  exports: [
    HerdComponent,
    MenuComponent,
    FsEntryComponent,
    NewTaskModalComponent,
    NewContainerModalComponent,
    DefaultRolesModalComponent,
    DefaultTaskTypesModalComponent,
    IssueCardComponent,
    ContainerCardComponent,
    RaamCardComponent,
    RaamInputComponent,
    RaamSwitchComponent,
    RaamHeadlineComponent,
    RaamDropDownComponent,
    UserCardComponent,
    UserMiniComponent,
    ScrumCardComponent,
    RaamColorComponent,
    DefaultCollapsibleComponent,
    AssignmentCollapsibleComponent,
    AssigneeCollapsibleComponent,
    ParentCollapsibleComponent,
    ChildrenCollapsibleComponent,
    FilePathCollapsibleComponent,
    BranchNameCollapsibleComponent,
    ContainerCollapsibleComponent,
    ContainerPillComponent,
    FileCardComponent,
    RepoCradComponent,
    NewCredentialsModalComponent,
  ],
  declarations: [
    HerdComponent,
    MenuComponent,
    FsEntryComponent,
    NewTaskModalComponent,
    NewContainerModalComponent,
    DefaultRolesModalComponent,
    DefaultTaskTypesModalComponent,
    IssueCardComponent,
    ContainerCardComponent,
    RaamCardComponent,
    RaamInputComponent,
    RaamSwitchComponent,
    RaamHeadlineComponent,
    RaamDropDownComponent,
    UserCardComponent,
    UserMiniComponent,
    ScrumCardComponent,
    RaamSwitchComponent,
    RaamColorComponent,
    DefaultCollapsibleComponent,
    AssignmentCollapsibleComponent,
    AssigneeCollapsibleComponent,
    ParentCollapsibleComponent,
    ChildrenCollapsibleComponent,
    FilePathCollapsibleComponent,
    BranchNameCollapsibleComponent,
    ContainerCollapsibleComponent,
    ContainerPillComponent,
    FileCardComponent,
    RepoCradComponent,
    NewCredentialsModalComponent,
  ],
})
export class SharedModule {
}
