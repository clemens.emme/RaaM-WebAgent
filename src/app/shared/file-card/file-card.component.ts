import {Component, Input, OnInit} from '@angular/core';
import {ContainerModel} from '../../api/model/containerModel';

@Component({
  selector: 'fileCard',
  templateUrl: './file-card.component.html',
  styleUrls: ['./file-card.component.css']
})
export class FileCardComponent implements OnInit {

  @Input() verbose = false;
  @Input() parameters: any;
  @Input() container: ContainerModel;
  @Input() path: string;

  constructor() { }

  ngOnInit() {
  }

  public displayFilePathAsFileLink() {
    return this.isExistingFileEndpoint() && this.verbose;
  }

  public displayFilePathAsToBeGeneratedFile() {
    return !this.isExistingFileEndpoint()  && this.verbose;
  }

  public isExistingFileEndpoint() {
    if(!this.path) return false;
    return !this.path.endsWith('/');
  }
}
