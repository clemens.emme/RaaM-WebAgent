import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'fsEntry',
  templateUrl: './fs-entry.component.html',
  styleUrls: ['./fs-entry.component.css']
})
export class FsEntryComponent implements OnInit {

  public DIR_TYPE = 'dir';
  public FILE_TYPE = 'file';

  @Input() name: string;
  @Input() type: string;
  @Input() sub: [object];

  @Output() chosen: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public addOwnDir(subDir) {
    this.returnValue(this.name + '/' + subDir);

  }

  public returnValue(name) {
    this.chosen.emit(name);
  }

}
