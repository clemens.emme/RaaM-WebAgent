import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsEntryComponent } from './fs-entry.component';

describe('FsEntryComponent', () => {
  let component: FsEntryComponent;
  let fixture: ComponentFixture<FsEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
