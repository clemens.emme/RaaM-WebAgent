import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamSwitchComponent } from './raam-switch.component';

describe('RaamSwitchComponent', () => {
  let component: RaamSwitchComponent;
  let fixture: ComponentFixture<RaamSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
