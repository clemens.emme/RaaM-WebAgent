import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'raamSwitch',
  templateUrl: './raam-switch.component.html',
  styleUrls: ['./raam-switch.component.css']
})
export class RaamSwitchComponent implements OnInit {

  @Input() label = 'Title here';
  @Input() value = false;
  @Input() color = '#2196F3';
  @Output() valueChange = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  onChange(e) {
    this.valueChange.emit(e.target.checked);
  }
}
