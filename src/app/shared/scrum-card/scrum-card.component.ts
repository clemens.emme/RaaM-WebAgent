import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'scrumCard',
  templateUrl: './scrum-card.component.html',
  styleUrls: ['./scrum-card.component.css']
})
export class ScrumCardComponent implements OnInit {

  @Input() value: number;
  @Input() size = 10;

  @Output() output: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
  }

  ngOnInit() {
  }

  public returnOutput() {
    this.output.emit(this.value);
  }

  fullSize(ratio) {
    if(!ratio) ratio = 1;
    return this.size * ratio;
  }

  halfSize(ratio) {
    if(!ratio) ratio = 1;
    return (this.size / 1.5) * ratio;
  }
}
