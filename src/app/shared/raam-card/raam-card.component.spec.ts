import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaamCardComponent } from './raam-card.component';

describe('RaamCardComponent', () => {
  let component: RaamCardComponent;
  let fixture: ComponentFixture<RaamCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaamCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaamCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
