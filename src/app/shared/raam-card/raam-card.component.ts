import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'raamCard',
  templateUrl: './raam-card.component.html',
  styleUrls: ['./raam-card.component.css']
})
export class RaamCardComponent implements OnInit{

  @Input() title: string;
  @Input() menu = false;
  @Input() fxLayout = 'start';
  @Input() action;
  @Input() current: boolean;

  constructor() {
  }

  ngOnInit() {
  }

  callAction() {
    if(!this.action) return;
    this.action();
  }
}
