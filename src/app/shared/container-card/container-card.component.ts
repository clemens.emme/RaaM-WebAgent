import {Component, Input, OnInit} from '@angular/core';
import {ContainerService} from '../../api/services/container/container.service';
import {ContainerModel} from '../../api/model/containerModel';

@Component({
  selector: 'containerCard',
  templateUrl: './container-card.component.html',
  styleUrls: ['./container-card.component.css']
})
export class ContainerCardComponent implements OnInit {

  @Input() container: ContainerModel;

  constructor(private containerService: ContainerService) {
  }

  ngOnInit() {
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected putContainer() {
    this.containerService.putOne(this.container).then(res => {
      this.container = res;
    });
  }
}
