import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {Repository} from '../../api/model/repository';
import {RepositoryService} from '../../api/services/repository/repository.service';
import {GitService} from '../../api/services/git/git-service.service';
import {Router} from '@angular/router';
import {UserService} from '../../api/services/user/user.service';
import {NewCredentialsModalComponent} from '../modals/new-credentials-modal/new-credentials-modal.component';

@Component({
  selector: 'repoCrad',
  templateUrl: './repo-crad.component.html',
  styleUrls: ['./repo-crad.component.css']
})
export class RepoCradComponent implements OnInit {
  @ViewChild('credentialModal') credentialModal: NewCredentialsModalComponent;

  @Input() repo: Repository;
  @Input() name: string;

  @Output() noDbEntryCallback: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    public router: Router,
    public gitService: GitService,
    public repoService: RepositoryService,
    public userService: UserService) {
  }

  ngOnInit() {
  }

  // ==================================================================================================================================
  // Git Data Methods
  // ==================================================================================================================================

  public gitGetOneRepo(name) {
    if (!this.userService.getUser().getCredentialsForRepo(this.repo)) {
      this.credentialModal.open(this.repo);
      return;
    }

    this.realGitGet(name);
  }

  public realGitGet(name) {
    this.repoService.getOneByName(name).then(res => {
      if (res) {
        this.router.navigateByUrl('/repository/taskList');
      } else {
        this.noDbEntryCallback.emit();
      }
    });
  }

  public gitClone() {
    if (!this.userService.getUser().getCredentialsForRepo(this.repo)) {
      this.credentialModal.open(this.repo, this.repo.url);
      return;
    }

    this.gitService.cloneOneRepoByContext(this.repo).then(res => {
      this.name = this.repo.name;
    });
  }
}
