import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RepoCradComponent } from './repo-crad.component';

describe('RepoCradComponent', () => {
  let component: RepoCradComponent;
  let fixture: ComponentFixture<RepoCradComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RepoCradComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RepoCradComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
