export class ModelChangeListener {

  private listeners = [];

  // Method to be callt to propergate the model change
  protected notifyAll() {
    this.listeners.forEach(callBack => callBack.call());
  }

  // Creating a new listener bei input and add it to intern list
  public registerListener(name: string, call: any) {
    this.addListener({name: name, call: call});
  }

  // Adding extern listener to the intern List
  public addListener(listener: any) {
    if (this.listeners.findIndex(x => x.name === listener) < 0) {
      this.listeners.push(listener);
    }
  }

  public unregisterListener(name) {
    const index = this.listeners.findIndex(i => i.from === name);
    if (index >= 0) {
      this.listeners.splice(index, 1);
    }
  }
}
