import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class HttpService {

  constructor(private http: Http,
              private httpClient: HttpClient) {
  }

  getHeader(): Headers {
    return new Headers({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'charset': 'UTF-8'
    });
  }

  getBlobHeader(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'charset': 'UTF-8',
    });
  }

  // ==================================================================================================================================
  // Http Methods
  // ==================================================================================================================================

  get(url): Promise<any> {
    return this.http.get(url, new RequestOptions({headers: this.getHeader()})).toPromise();
    // return this.httpClient.get(url, { headers: this.getHttpHeader() }).toPromise();
  }

  getBlob(url): Promise<any> {
    return this.httpClient.get(url, {responseType: 'blob'}).toPromise();
  }

  post(url, data): Promise<any> {
    return this.http.post(url, data, new RequestOptions({headers: this.getHeader()})).toPromise();

  }

  postBlob(url, data): Promise<any> {
    return this.httpClient.post(url, data, {headers: this.getBlobHeader(), responseType: 'blob' as 'json'}).toPromise();
  }

  put(url, data): Promise<any> {
    return this.http.put(url, data, new RequestOptions({headers: this.getHeader()})).toPromise();
  }


  delete(url): Promise<any> {
    return this.http.delete(url, new RequestOptions({headers: this.getHeader()})).toPromise();
  }

  // ==================================================================================================================================
  // Local Methods
  // ==================================================================================================================================

  getLocalFile(url): Promise<any> {
    return this.httpClient.get(url).toPromise();
  }
}
