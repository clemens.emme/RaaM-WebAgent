import {Injectable} from '@angular/core';
import {ContainerModel} from '../../model/containerModel';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';
import {saveAs} from 'file-saver';

@Injectable()
export class ContainerService {

  constructor(private http: HttpService) {
  }

  // ==================================================================================================================================
  // Container Interaction
  // ==================================================================================================================================

  public async getOneContainerConfig(url): Promise<any> {
    const URL = url + environment.apiPrefix;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async getOneContainerTestFile(container: ContainerModel): Promise<any> {
    const URL = container.url + +environment.apiPrefix + environment.contGenerate;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async postOneContainerParameters(parameter: any, container: ContainerModel): Promise<any> {
    const URL = container.url + environment.apiPrefix + environment.contGenerate;
    return await this.http.postBlob(URL, JSON.stringify(parameter));
  }

  // TODO find out how to transmit files
  public async putOneContainerFile(file: object | Blob, container: ContainerModel): Promise<any> {
    const URL = container.url + +environment.apiPrefix + environment.contGenerate;
    const response = await this.http.post(URL, JSON.stringify(file));

    return response.json();
  }

  // ==================================================================================================================================
  // REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.container;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.container + '/' + id;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async postOne(container: ContainerModel): Promise<any> {
    if (this.containerIsIncomplete(container)) return;
    const URL = environment.dbServiceURL + environment.container;
    const response = await this.http.post(URL, JSON.stringify(container));

    return response.json();
  }

  public async putOne(container: ContainerModel): Promise<any> {
    const URL = environment.dbServiceURL + environment.container + '/' + container._id;
    const response = await this.http.put(URL, JSON.stringify(container));

    return response.json();
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.container + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Test Methods
  // ==================================================================================================================================

  public blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;

    b.lastModifiedDate = new Date();
    b.name = fileName;

    return <File>theBlob;
  }

  public saveToFileSystem(theBlob: Blob, fileName: string, container: ContainerModel) {
    const name = fileName + '.' + container.format;
    const file = this.blobToFile(theBlob, name);
    saveAs(file);
  }

  public containerIsIncomplete(container: ContainerModel) {
    return false;
  }
}
