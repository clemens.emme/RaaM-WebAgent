import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';
import {TaskType} from '../../model/task-type';

@Injectable()
export class TaskTypeService {

  constructor(private http: HttpService) {
  }

  // ==================================================================================================================================
  // REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.taskType;
    const response = await this.http.get(URL);

    // Dose Not need parsing, because the lack of important methods in the class
    return response.json();
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.taskType + '/' + id;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async postOne(taskType: TaskType): Promise<any> {
    if (this.taskTypeIsIncomplete(taskType)) return;
    const URL = environment.dbServiceURL + environment.taskType;
    const response = await this.http.post(URL, JSON.stringify(taskType));

    return response.json();
  }

  public async putOne(taskType: TaskType): Promise<any> {
    const URL = environment.dbServiceURL + environment.taskType + '/' + taskType._id;
    const response = await this.http.put(URL, JSON.stringify(taskType));

    return response.json();
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.taskType + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Test Methods
  // ==================================================================================================================================

  public taskTypeIsIncomplete(taskType: TaskType) {
    return false;
  }

}
