import {Injectable} from '@angular/core';
import {ModelChangeListener} from '../utils/listener';
import {Repository} from '../../model/repository';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';
import {Person} from '../../model/person';
import {Task} from '../../model/task';

@Injectable()
export class RepositoryService extends ModelChangeListener {

  private repository: Repository;

  constructor(private http: HttpService) {
    super();
  }

  // ==================================================================================================================================

  public setRepo(repository: Repository) {
    this.repository = repository;
    this.notifyAll();
  }

  public resetRepo() {
    this.setRepo(undefined);
  }

  public getRepo() {
    return this.repository;
  }

  // ==================================================================================================================================

  public async getOneByName(name: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.repository + '/' + name;
    const response = await this.http.get(URL);

    this.setRepo(response.json());

    return response.json();
  }

  // ==================================================================================================================================
  // REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.repository;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.repository + '/' + id;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async postOne(repo: Repository): Promise<any> {
    if (this.canCreateRepo(repo)) return;
    const URL = environment.dbServiceURL + environment.repository;
    const response = await this.http.post(URL, JSON.stringify(repo));
    return response.json();
  }

  public async putOne(repo: Repository): Promise<any> {
    const URL = environment.dbServiceURL + environment.repository + '/' + repo._id;
    const response = await this.http.put(URL, JSON.stringify(repo));

    return response.json();
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.repository + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Test Methods
  // ==================================================================================================================================

  public canCreateRepo(repo: Repository) {
    return false;
  }
}
