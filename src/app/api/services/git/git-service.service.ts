import {Injectable} from '@angular/core';
import {HttpService} from '../utils/http-service.service';
import {environment} from '../../../../environments/environment';
import {FileUploader} from 'ng2-file-upload';
import {ModelChangeListener} from '../utils/listener';
import {Repository} from '../../model/repository';
import {UserService} from '../user/user.service';
import {RepositoryService} from '../repository/repository.service';
import {Person} from '../../model/person';
import {Task} from '../../model/task';

@Injectable()
export class GitService extends ModelChangeListener {

  public uploader: FileUploader = new FileUploader({
    url: environment.gitServiceURL + environment.add,
    isHTML5: true,
    method: 'POST',
    itemAlias: 'file',
  });

  private loggedInUser: Person;
  private currentRepo: Repository;

  public getUploader() {
    return this.uploader;
  }


  constructor(
    private http: HttpService,
    private userService: UserService,
    private repositoryService: RepositoryService) {
    super();

    this.userService.registerListener('GitService', () => this.getCurrentRepo());
    this.repositoryService.registerListener('GitService', () => this.getLoggedInUser());
  }

  protected getCurrentRepo() {
    this.currentRepo = this.repositoryService.getRepo();
  }

  protected getLoggedInUser() {
    this.loggedInUser = this.userService.getUser();
  }

  protected buildInfoPackage(repo: Repository, user: Person, option: any) {
    if (!repo) repo = this.repositoryService.getRepo();
    if (!user) user = this.userService.getUser();

    const credentials = user.getCredentialsForRepo(repo);

    return {
      path: option ? option.path : '',
      branch: option ? option.branch : '',
      gitUrl: repo ? repo.url : '',
      fileName: option ? option.fileName : '',
      username: option ? (option.username ? option.username : (credentials ? credentials.username : '')) : (credentials ? credentials.username : ''),
      password: option ? (option.password ? option.password : (credentials ? credentials.password : '')) : (credentials ? credentials.password : ''),
      projectName: repo ? repo.name : '',
    };

  }

  // ==================================================================================================================================
  // File Upload
  // ==================================================================================================================================

  public uploadBlob(blob: Blob, fileName: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.uploader.onAfterAddingFile = (file) => {
        file.withCredentials = false;
      };
      this.uploader.onCompleteItem = () => resolve();
      this.uploader.onErrorItem = () => reject();


      this.uploader.addToQueue(new Array<File>(this.blobToFile(blob, fileName)));
      this.uploader.uploadAll();
    });
  }

  public blobToFile(theBlob: Blob, fileName: string): File {
    const b: any = theBlob;

    b.lastModifiedDate = new Date();
    b.name = fileName;

    return <File>theBlob;
  }

  // ==================================================================================================================================
  // Repo Names
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     X
  //   branch:    X
  //   gitUrl:    X
  //   fileName:  X
  //   username:   X
  //   password:   X
  //   projectName: X

  // Get Cloned Repo Names
  async getRepos(): Promise<any> {
    const URL = environment.gitServiceURL;
    const response = await this.http.get(URL);
    return response.json();
  }

  // ==================================================================================================================================
  // History
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     X
  //   branch:    X
  //   gitUrl:    string
  //   fileName:  X
  //   username:   string
  //   password:   string
  //   projectName: string

  // Git History By global Context
  async getOneRepoByContext(): Promise<any> {
    return this.getOneReposCommitHistory(this.repositoryService.getRepo());
  }

  // Git History
  async getOneReposCommitHistory(repo: Repository): Promise<any> {
    const infoPackage = this.buildInfoPackage(repo, null, null);

    const URL = environment.gitServiceURL + environment.repo + '/' + repo.name + environment.history;
    const response = await this.http.put(URL, JSON.stringify(infoPackage));
    return response.json();
  }

  // ==================================================================================================================================
  // Branches
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     X
  //   branch:    X
  //   gitUrl:    string
  //   fileName:  X
  //   username:   string
  //   password:   string
  //   projectName: string

  // Get Branches Info of Repo By Saved Data
  async getOneReposBranchListByContext(): Promise<any> {
    return this.getOneReposBranchList(this.repositoryService.getRepo());
  }

  // Get Branches Info of Repo
  async getOneReposBranchList(repo: Repository): Promise<any> {
    const infoPackage = this.buildInfoPackage(repo, null, null);

    const URL = environment.gitServiceURL + environment.repo + '/' + repo.name + environment.branch;
    const response = await this.http.put(URL, JSON.stringify(infoPackage));
    return response.json();
  }

  // ==================================================================================================================================
  // File System
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     X
  //   branch:    string (optional)
  //   gitUrl:    string
  //   fileName:  X
  //   username:   string
  //   password:   string
  //   projectName: string

  // Get File System of Repo without options
  async getOneReposFileSystemByContext(): Promise<any> {
    return this.getOneReposFileSystemByContextAndBranch(null);
  }

  // Get File System of Repo with options (branchname)
  async getOneReposFileSystemByContextAndBranch(option: any): Promise<any> {
    return this.getOneReposFileSystem(this.repositoryService.getRepo(), option);
  }

  // Get File System of Repo
  async getOneReposFileSystem(repo: Repository, option: any): Promise<any> {
    const infoPackage = this.buildInfoPackage(repo, null, option);

    const URL = environment.gitServiceURL + environment.repo + '/' + repo.name + environment.files;
    const response = await this.http.put(URL, JSON.stringify(infoPackage));
    return response.json();
  }

  // ==================================================================================================================================
  // Clone
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     X
  //   branch:    X
  //   gitUrl:    string
  //   fileName:  X
  //   username:   string
  //   password:   string
  //   projectName: X

  public async cloneOneRepoByContextAndUrlAndCredentials(url: string, username: string, password: string): Promise<any> {
    return this.cloneOneRepo({url: url} as Repository, this.userService.getUser(), {username: username, password: password});
  }

  public async cloneOneRepoByContextAndUrl(url: string): Promise<any> {
    return this.cloneOneRepoByContext({url: url} as Repository);
  }

  public async cloneOneRepoByContext(repo: Repository): Promise<any> {
    return this.cloneOneRepo(repo, this.userService.getUser(), null);
  }

  // Clone Repo
  public async cloneOneRepo(repo: Repository, user: Person, option: any): Promise<any> {
    const infoPackage = this.buildInfoPackage(repo, user, option);

    const URL = environment.gitServiceURL;
    const response = await this.http.post(URL, JSON.stringify(infoPackage));

    return response.json();
  }

  // ==================================================================================================================================
  // Commit
  // ==================================================================================================================================
  //
  //   Request Data :
  //   --------------
  //   path:     string
  //   branch:    string
  //   gitUrl:    string
  //   fileName:  string
  //   username:   string
  //   password:   string
  //   projectName: string

  // Add a commit to the Repo by saved Data
  public async postOneCommitByContext(task: Task): Promise<any> {
    return this.addFileAndCommit(this.repositoryService.getRepo(), this.userService.getUser(), task);
  }

  // Add a commit to the Repo
  public async addFileAndCommit(repo: Repository, user: Person, task: Task): Promise<any> {
    const infoPackage = this.buildInfoPackage(repo, user, this.buildOptionFromTask(task));

    const URL = environment.gitServiceURL + environment.repo + '/' + repo.name + environment.commit;
    const response = await this.http.post(URL, JSON.stringify(infoPackage));

    return response.json();
  }

  private buildOptionFromTask(task: Task) {
    const split = task.filePath.split('/');
    return {
      path: task.filePath,
      branch: task.branchName,
      fileName: split[split.length - 1],
    };
  }
}
