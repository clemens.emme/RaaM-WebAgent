import {Injectable} from '@angular/core';
import {Role} from '../../model/role';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';

@Injectable()
export class RoleService {

  private rolls: Role[] = [
    new Role().setDefaultValues(),
    new Role().setValues('Developer', '#d9534f'),
  ];

  constructor(private http: HttpService) {
  }

  // ==================================================================================================================================
  // REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.role;
    const response = await this.http.get(URL);

    // Dose Not need parsing, because the lack of important methods in the class
    return response.json();
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.role + '/' + id;
    const response = await this.http.get(URL);

    return response.json();
  }

  public async postOne(role: Role): Promise<any> {
    if (this.roleIsIncomplete(role)) return;
    const URL = environment.dbServiceURL + environment.role;
    const response = await this.http.post(URL, JSON.stringify(role));

    return response.json();
  }

  public async putOne(role: Role): Promise<any> {
    const URL = environment.dbServiceURL + environment.role + '/' + role._id;
    const response = await this.http.put(URL, JSON.stringify(role));

    return response.json();
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.role + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Test Methods
  // ==================================================================================================================================

  public roleIsIncomplete(role: Role) {
    return false;
  }
}
