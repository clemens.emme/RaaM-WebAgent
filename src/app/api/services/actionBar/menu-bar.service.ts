import {Injectable} from '@angular/core';
import {ModelChangeListener} from '../utils/listener';
import {Person} from '../../model/person';
import {Repository} from '../../model/repository';

@Injectable()
export class MenuBarService extends ModelChangeListener {

  private secretActions = [];
  private routingActions = [];
  private displayUser: Person;
  private displayRepo: Repository;

  // ==================================================================================================================================
  // Secret Actions
  // ==================================================================================================================================
  public setSecretActions(newSecrets) {
    this.secretActions = newSecrets;
    this.notifyAll();
  }

  public getSecretActions() {
    return this.secretActions;
  }

  // ==================================================================================================================================
  // Leading User Item
  // ==================================================================================================================================
  public setDisplayUser(user: Person) {
    this.displayUser = user;
    this.notifyAll();
  }

  public resetDisplayUser() {
    this.setDisplayUser(undefined);
  }

  public getDisplayUser() {
    return this.displayUser;
  }

  // ==================================================================================================================================
  // Leading Repo Item
  // ==================================================================================================================================
  public setDisplayRepo(repo: Repository) {
    this.displayRepo = repo;
    this.notifyAll();
  }

  public resetDisplayRepo() {
    this.setDisplayRepo(undefined);
  }

  public getDisplayRepo() {
    return this.displayRepo;
  }


  // ==================================================================================================================================
  // Routing Actions
  // ==================================================================================================================================
  public setRoutingActions(newActions) {
    this.routingActions = newActions;
    this.notifyAll();
  }

  public getRoutingActions() {
    return this.routingActions;
  }

}
