import {Injectable} from '@angular/core';
import {Task} from '../../model/task';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';
import {WorkLog} from '../../model/work-log';

@Injectable()
export class TaskService {

  constructor(private http: HttpService) {
  }

  // ==================================================================================================================================
  // REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.task;
    const response = await this.http.get(URL);

    // Dose Not need parsing, because the lack of important methods in the class
    return response.json();
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.task + '/' + id;
    const response = await this.http.get(URL);
    const task = Object.assign(new Task, response.json());
    task.workLogs = task.workLogs.map((log: WorkLog) => Object.assign(new WorkLog, log));
    return task;
  }

  public async postOne(task: Task): Promise<any> {
    if (this.taskIsIncomplete(task)) return;
    const URL = environment.dbServiceURL + environment.task;
    const response = await this.http.post(URL, JSON.stringify(task));

    return response.json();
  }

  public async putOne(task: Task): Promise<any> {
    const URL = environment.dbServiceURL + environment.task + '/' + task._id;
    const response = await this.http.put(URL, JSON.stringify(task));

    return response.json();
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.task + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Test Methods
  // ==================================================================================================================================

  public taskIsIncomplete(task: Task) {
    return false;
  }
}
