import {Injectable} from '@angular/core';
import {ModelChangeListener} from '../utils/listener';
import {Person} from '../../model/person';
import {environment} from '../../../../environments/environment';
import {HttpService} from '../utils/http-service.service';
import {noUndefined} from '@angular/compiler/src/util';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class UserService extends ModelChangeListener {

  private user: Person;

  constructor(private http: HttpService) {
    super();
  }

  // ==================================================================================================================================

  public setUser(user: Person) {
    this.user = user;
    this.notifyAll();
  }

  public getUser() {
    return this.user;
  }

  public isLoggedIn() {
    return !!this.user;
  }

  public async getMainUser(): Promise<any> {
    return await this.http.getLocalFile('./assets/realUser.json');
  }

  // ==================================================================================================================================
  // Own User REST CRUD
  // ==================================================================================================================================

  public isLoggedInUser(id: string): boolean {
    return id === this.user._id;
  }

  // loginData = { username: '', password: '' };
  public async login(loginData: object): Promise<any> {
    const URL = environment.dbServiceURL + environment.person + environment.login;
    const response = await this.http.post(URL, JSON.stringify(loginData));

    if (response.json() === undefined || response.json() === null) {
      return undefined;
    }

    this.setUser(Object.assign(new Person, response.json())); // parse Json To Person-Objects and set as logged in

    return this.getUser();
  }

  public async logout(): Promise<any> {
    this.setUser(undefined);
    return {}; // TODO make better return
  }

  public async putOwn(user: Person): Promise<any> {
    const URL = environment.dbServiceURL + environment.person + '/' + user._id;
    const response = await this.http.put(URL, JSON.stringify(user));

    this.setUser(Object.assign(new Person, response.json())); // parse Json To Person-Objects and set as logged in

    return this.getUser();
  }

  // ==================================================================================================================================
  // Normal User REST CRUD
  // ==================================================================================================================================

  public async getAll(): Promise<any> {
    const URL = environment.dbServiceURL + environment.person;
    const response = await this.http.get(URL);

    return response.json().map((person: Person) => Object.assign(new Person, person)); // parse list of Json To List of Person-Objects
  }

  public async getOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.person + '/' + id;
    const response = await this.http.get(URL);

    return Object.assign(new Person, response.json()); // parse Json To Person-Objects
  }

  public async postOne(user: Person): Promise<any> {
    if (this.userIsIncomplete(user)) return;
    const URL = environment.dbServiceURL + environment.person;
    const response = await this.http.post(URL, JSON.stringify(user));

    return Object.assign(new Person, response.json()); // parse Json To Person-Objects
  }

  public async putOne(user: Person): Promise<any> {
    const URL = environment.dbServiceURL + environment.person + '/' + user._id;
    const response = await this.http.put(URL, JSON.stringify(user));

    return Object.assign(new Person, response.json()); // parse Json To Person-Objects
  }

  public async deleteOne(id: string): Promise<any> {
    const URL = environment.dbServiceURL + environment.person + '/' + id;
    const response = await this.http.delete(URL);

    return response.json();
  }

  // ==================================================================================================================================
  // Helper Methods
  // ==================================================================================================================================

  public userIsIncomplete(user: Person) {
    return false;
  }
}
