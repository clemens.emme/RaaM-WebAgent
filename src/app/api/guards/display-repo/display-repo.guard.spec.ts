import { TestBed, async, inject } from '@angular/core/testing';

import { DisplayRepoGuard } from './display-repo.guard';

describe('DisplayRepoGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DisplayRepoGuard]
    });
  });

  it('should ...', inject([DisplayRepoGuard], (guard: DisplayRepoGuard) => {
    expect(guard).toBeTruthy();
  }));
});
