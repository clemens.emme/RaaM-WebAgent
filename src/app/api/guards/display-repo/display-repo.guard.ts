import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {MenuBarService} from '../../services/actionBar/menu-bar.service';
import {RepositoryComponent} from '../../../repository/repository.component';
import {RepositoryService} from '../../services/repository/repository.service';

@Injectable()
export class DisplayRepoGuard implements CanActivate, CanDeactivate<RepositoryComponent> {
  constructor(
    private router: Router,
    private menuBarService: MenuBarService,
    private repoService: RepositoryService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (this.repoService.getRepo()) {
      return true;
    }
    this.router.navigate(['home']);
    return false;
  }

  // happens on leaving the general repository route in the app.module
  canDeactivate(component: RepositoryComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot,
                nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.menuBarService.resetDisplayRepo();
    return true;
  }
}
