import { TestBed, async, inject } from '@angular/core/testing';

import { DisplayUserGuard } from './display-user.guard';

describe('DisplayUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DisplayUserGuard]
    });
  });

  it('should ...', inject([DisplayUserGuard], (guard: DisplayUserGuard) => {
    expect(guard).toBeTruthy();
  }));
});
