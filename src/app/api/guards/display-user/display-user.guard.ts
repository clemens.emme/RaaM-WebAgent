import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UsersComponent} from '../../../users/users.component';
import {MenuBarService} from '../../services/actionBar/menu-bar.service';
import {UserService} from '../../services/user/user.service';

@Injectable()
export class DisplayUserGuard implements CanActivate, CanDeactivate<UsersComponent> {
  constructor(
    private router: Router,
    private menuBarService: MenuBarService,
    private userService: UserService) {
  }

  // happens on entering Users, User, UserCreation, UserCreations
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.userService.getUser()) {
      return true;
    }
    this.router.navigate(['home']);
    return false;
  }

  // happens on leaving the general users route in the app.module
  canDeactivate(component: UsersComponent, currentRoute: ActivatedRouteSnapshot, currentState: RouterStateSnapshot,
                nextState: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    this.reset();
    return true;
  }

  public reset() {
    this.menuBarService.resetDisplayUser();
  }
}
