import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {UserService} from '../../services/user/user.service';
import {environment} from '../../../../environments/environment';

@Injectable()
export class AppGuard implements CanActivate {

  constructor(private userService: UserService, public router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.userService.isLoggedIn() || environment.test) {
      return true;
    }
    this.router.navigate(['home']);
    return false;
  }
}
