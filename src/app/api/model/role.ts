import {DbObject} from './utils/db-object';
import {environment} from '../../../environments/environment';

export class Role  extends DbObject  {
  public name: string;
  public color: string;

  setValues(name: string, color: string) {
    this.name = name;
    this.color = color;

    return this;
  }

  // ==================================================================================================================================
  // Test Data
  // ==================================================================================================================================

  public setDefaultValues() {
    this.name = 'Artist';
    this.color = '#007cff';

    return this;
  }

  public setToArtistDefault() {
    this.name = 'Artist';
    this.color = environment.artistColor;

    return this;
  }

  public setToDeveloperDefault() {
    this.name = 'Developer';
    this.color = environment.developerColor;

    return this;
  }
}
