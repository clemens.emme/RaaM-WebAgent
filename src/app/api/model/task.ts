import {WorkLog} from './work-log';
import {Person} from './person';
import {DbObject} from './utils/db-object';
import {TaskType} from './task-type';
import {ContainerModel} from './containerModel';

export class Task extends DbObject {
  public static MINUTE = 60;
  public static HOURS = 24;

  public title: string;
  public filePath: string;
  public branchName: string;
  public description: string;
  public estimatedPoints: number;

  public type: TaskType;
  public container: ContainerModel;
  public assignee: Person;
  public parent: Task;
  public children: Task[];
  public workLogs: WorkLog[];


  // ==================================================================================================================================
  //
  // Test Data
  //
  // ==================================================================================================================================

  public setTitle(title: string) {
    this.title = title;

    return this;
  }

  public setDefaultValues() {
    this._id = '1337';
    this.type = new TaskType().setDefaultValues();
    this.container = new ContainerModel().setDefaultValues();
    this.filePath = 'src/main/resources/de/uniks/assets';
    this.estimatedPoints = 13;
    this.title = 'Create easter nest as endpoints';
    this.description =
      'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.' +
      'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. ' +
      'Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. ' +
      'Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ' +
      'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. ' +
      'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat.';
    // Assignee
    this.assignee = new Person().setDefaultValues();
    // Work Done
    this.branchName = 'EasterNestEndpoints';
    this.workLogs = [
      new WorkLog().setDefaultValues(),
      new WorkLog().setDefaultValues(),
      new WorkLog().setDefaultValues(),
      new WorkLog().setDefaultValues(),
    ];

    return this;
  }
}
