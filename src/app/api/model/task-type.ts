import {DbObject} from './utils/db-object';
import {environment} from '../../../environments/environment';

export class TaskType extends DbObject {
  public static TYPE_EPIC = 'Epic';
  public static TYPE_STORY = 'Story';
  public static TYPE_SUB_TASK = 'Sub Task';
  public static TYPE_BUG = 'Bug';
  public static TYPE_ASSET = 'Asset';
  public static TYPE_LIST = [TaskType.TYPE_EPIC, TaskType.TYPE_STORY, TaskType.TYPE_BUG, TaskType.TYPE_SUB_TASK, TaskType.TYPE_ASSET];

  public name: string;
  public color: string;
  public canLinkFile: boolean;

  setValues(name: string, color: string, canLinkFile: boolean) {
    this.name = name;
    this.color = color;
    this.canLinkFile = canLinkFile;

    return this;
  }

  // ==================================================================================================================================
  // Test Data
  // ==================================================================================================================================

  public setDefaultValues() {
    this.name = TaskType.TYPE_BUG;
    this.color = environment.bugColor;

    return this;
  }

  setToEpicDefault() {
    this.name = TaskType.TYPE_EPIC;
    this.color = environment.epicColor;
    this.canLinkFile = false;

    return this;
  }

  setToStoryDefault() {
    this.name = TaskType.TYPE_STORY;
    this.color = environment.storyColor;
    this.canLinkFile = false;

    return this;
  }

  setToSubTaskDefault() {
    this.name = TaskType.TYPE_SUB_TASK;
    this.color = environment.subTaskColor;
    this.canLinkFile = true;

    return this;
  }

  setToBugDefault() {
    this.name = TaskType.TYPE_BUG;
    this.color = environment.bugColor;
    this.canLinkFile = true;

    return this;
  }

  setToAssetDefault() {
    this.name = TaskType.TYPE_ASSET;
    this.color = environment.assetColor;
    this.canLinkFile = true;

    return this;
  }
}
