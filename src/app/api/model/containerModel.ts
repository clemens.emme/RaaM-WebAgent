import {DbObject} from './utils/db-object';

export class ContainerModel extends DbObject {

  public name: string; // generic name of the Container. eg. JavaClassGenService
  public format: string; // Asset Type Name, eg. Epic, Story, Task, PNG, Java // TODO Returned by the Container
  public color: string; // Color to be represented in the applications
  public description: string; // Description of the generated file or other notable details

  public url: string; // the base url to call this container // TODO Choosen by user

  public parameters: object[]; // All parameters editable by the Service // TODO Returned by the Container

  public isValidContainer() {
    return this.format && this.name && this.color && this.parameters;
  }

  // ==================================================================================================================================

  // 4 api endpoints max needed

  // 1. Base route of api should return the configuration of the container
  // eg. which type is return and what parameter it can handle
  // GET http://avocado.uniks.de:13346/api

  // 2. The route that return a file by the given parameter
  // minimum number of parameters are one that must be the name of the file
  // post http://avocado.uniks.de:13346/api/generate

  // 3. The route that return a test file with no given parameter
  // get http://avocado.uniks.de:13346/api/generate

  // 5. The route that return a file by a given file
  // put http://avocado.uniks.de:13346/api/generate

  // ==================================================================================================================================
  // Test Data
  // ==================================================================================================================================

  public setDefaultValues() {
    this.name = 'Test Container';
    this.format = 'Test';
    this.color = '#ffcfff';
    this.description = 'This is only a Test-Container. It is only for display and view testing purposes.';

    this.url = 'http://localhost:1337';

    this.parameters = [
      {name: 'name', type: 'string', example: 'testForAll'},
      {name: 'width', type: 'number', example: '360'},
      {name: 'height', type: 'number', example: '720'},
    ];

    return this;
  }

  public setFirstTest() {
    this.name = 'PNG Container';
    this.format = 'png';
    this.color = '#ffcfff';
    this.parameters = [
      {name: 'name', type: 'string', example: 'blutig'},
      {name: 'width', type: 'number', example: 360},
      {name: 'height', type: 'number', example: 720},
      {name: 'color', type: 'string', example: '#133'},
    ];
    return this;
  }

  public setSecondTest() {
    this.name = 'JPG Container';
    this.format = 'jpg';
    this.color = '#ffcfff';
    this.parameters = [
      {name: 'name', type: 'string', example: 'button'},
      {name: 'color', type: 'string', example: '#133'},
    ];
    return this;
  }
}

const test = {
  'name': 'flag-service',
  'description': 'A service for controlling physical mail flags.',
  'author': 'Rügenwalder',
  'apiBase': 'http://avocado.uniks.de:13346/api',
  'services': [{
    'method': 'GET',
    'kind': 'timeseries',
    'endpoint': '/sensor',
    'description': 'TODO description /TODO',
    'example': 'TODO example /TODO'
  }, {
    'method': 'POST',
    'endpoint': '/sensor',
    'description': 'TODO description /TODO',
    'body': [{'label': 'timestamp', 'type': 'number'}, {'label': 'value', 'type': 'number'}],
    'example': 'TODO example /TODO'
  }, {
    'method': 'GET',
    'kind': 'single',
    'description': 'TODO description /TODO',
    'endpoint': '/sensor/:id',
    'param': [{'label': 'id', 'type': 'number'}],
    'example': 'TODO example /TODO'
  }, {
    'method': 'DELETE',
    'endpoint': '/sensor/:id',
    'description': 'TODO description /TODO',
    'param': [{'label': 'id', 'type': 'number'}],
    'example': 'TODO example /TODO'
  }, {
    'method': 'PATCH',
    'endpoint': '/sensor/:id',
    'description': 'TODO description /TODO',
    'param': [{'label': 'id', 'type': 'number'}],
    'example': 'TODO example /TODO'
  }, {
    'method': 'GET',
    'kind': 'action',
    'endpoint': '/action/raise',
    'description': 'TODO description /TODO',
    'actionLabel': 'Raise Flag',
    'example': 'TODO example /TODO'
  }, {
    'method': 'GET',
    'kind': 'action',
    'endpoint': '/action/lower',
    'description': 'TODO description /TODO',
    'actionLabel': 'Lower Flag',
    'example': 'TODO example /TODO'
  }]
};

const testTwo = {
  'name': 'Flag Administration',
  'description': 'A service for managing the physical mail flag services.',
  'author': 'Niklas Mollenhauer',
  'apiBase': 'http://avocado.uniks.de:13346/api',
  'websiteUrl': 'http://avocado.uniks.de:13346',
  'services': [{
    'method': 'GET',
    'kind': 'services',
    'endpoint': '/flag',
    'description': 'Retrieves a list of the currently available services.',
    'example': 'curl http://avocado.uniks.de:13346/api/flag'
  }, {
    'method': 'POST',
    'kind': 'service',
    'endpoint': '/flag',
    'description': 'Adds a new service.',
    'example': 'curl -X POST http://avocado.uniks.de:13346/api/flag'
  }, {
    'method': 'DELETE',
    'kind': 'service',
    'endpoint': '/flag/:id',
    'param': [{'type': 'string', 'label': 'id'}],
    'description': 'Deletes a service.',
    'example': 'curl -X DELETE http://avocado.uniks.de:13346/api/flag/abcdefghijklmnop'
  }, {
    'method': 'GET',
    'kind': 'string-list',
    'endpoint': '/sensor-id-suggestion',
    'description': 'Retrieves suggestions for sensor device IDs.',
    'example': 'curl http://avocado.uniks.de:13346/api/sensor-suggestion'
  }]
};
