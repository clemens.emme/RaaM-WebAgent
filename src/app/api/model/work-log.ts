import {DbObject} from './utils/db-object';

export class WorkLog extends DbObject {
  public static MINUTE = 60;
  public static HOURS = 24;

  public hoursSpend: number;
  public minutesSpend: number;
  public commitTitle: string;
  public commitDescription: string;

  createReadableLogTime() {
    const timeContainer = {days: 0, hours: this.hoursSpend, minutes: this.minutesSpend};
    let answer = '';

    // calculate the carrier
    timeContainer.hours += Math.floor(timeContainer.minutes / WorkLog.MINUTE); // rounding
    timeContainer.minutes = timeContainer.minutes % WorkLog.MINUTE;

    timeContainer.days += Math.floor(timeContainer.hours / WorkLog.HOURS); // rounding
    timeContainer.hours = timeContainer.hours % WorkLog.HOURS;

    if (timeContainer.days > 0) answer += timeContainer.days + 'd ';
    if (timeContainer.hours > 0) answer += timeContainer.hours + 'h ';
    if (timeContainer.minutes > 0) answer += timeContainer.minutes + 'm';

    return answer;
  }

  setDefaultValues() {
    this.hoursSpend = 2;
    this.minutesSpend = 4;
    this.commitTitle =
      'feat(Crate): added crate Object and build in into physic system';
    this.commitDescription =
      'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua.' +
      'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. ' +
      'Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';

    return this;
  }
}
