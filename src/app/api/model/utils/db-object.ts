export class DbObject {
  private readonly timeOption = {day: '2-digit', month: '2-digit', year: 'numeric'};

  public _id: string; // generic name of the Container. eg. JavaClassGenService
  public last_modified_date: string;
  public created_date: string;

  public getLastModifiedDateAsString() {
    return new Date(this.last_modified_date).toLocaleString('de-DE', this.timeOption);
  }

  public getCreationDateAsString() {
    return new Date(this.created_date).toLocaleString('de-DE', this.timeOption);

  }
}
