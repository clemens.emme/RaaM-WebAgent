export class Commit {
  public id: string;
  public author: string;
  public committer: string;
  public date: string;
  public message: string;
  public parentNo: number;
  public parents: string[];
  public summary: string;
  public time: string;
  public string: string;

  constructor() {
  }

  // ==================================================================================================================================
  //
  // Test Data
  //
  // ==================================================================================================================================

  setDefaultValues() {
    this.id = '64138768s4gs87g68s';
    this.author = 'Laurel';
    this.committer = 'Laurel';
    this.date = new Date().toDateString();
    this.message = 'feat(Feature): bla bla bla';
    this.parentNo = 2;
    this.parents = ['64138768s4gs87g68s', '64138768s4gs87666s'];
    this.summary = 'egiebguieigige';
    this.time = 'egiebguifebgifeigige';
    this.string = 'egiebguifebgife';

    return this;
  }
}
