import {Task} from './task';
import {DbObject} from './utils/db-object';

export class Repository  extends DbObject  {

  public name: string;
  public url: string;
  public tasks: Task[];

// ==================================================================================================================================
//
// Test Data
//
// ==================================================================================================================================


  public setDefaultValues() {
    this.name = 'Test_Repository';
    this.url = 'http://git-lab.com/Laurel/Test_Repository';
    this.tasks = [
      new Task().setDefaultValues(),
      new Task().setDefaultValues(),
      new Task().setDefaultValues(),
    ];
    return this;
  }
}
