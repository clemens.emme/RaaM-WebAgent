import {Role} from './role';
import {DbObject} from './utils/db-object';
import {Repository} from './repository';

export class Person extends DbObject {

  public username: string;
  public firstName: string;
  public lastName: string;
  public personalWords: string;

  public roles: Role[];
  public credentials: [{
    repository: Repository,
    username: string, // if Username and Password are not set, the person is just an observer
    password: string;
  }];

  public email: string;
  public password: string;

  public getDisplayName() {
    if (!this.firstName || !this.lastName) {
      return this.username;
    }
    return this.firstName + ' ' + this.lastName;
  }

  // ==================================================================================================================================
  // Credentials Management
  // ==================================================================================================================================

  public hasCredentialsForRepo(repo: Repository): boolean {
    return this.hasCredentialsForRepoName(repo.name);
  }

  public hasCredentialsForRepoName(name: string): boolean {
    return this.credentials.findIndex(cred => cred.repository && cred.repository.name === name) >= 0;
  }

  public getCredentialsForRepo(repo: Repository) {
    return this.getCredentialsForRepoName(repo.name);
  }

  public getCredentialsForRepoName(name: string) {
    const found = this.credentials.find(cred => cred.repository && cred.repository.name === name);
    return found;
  }

  // ==================================================================================================================================
  // Password Management
  // ==================================================================================================================================

  public setFirstPassword(password: string): string {
    if (!!this.password) return;
    this.password = password;
  }

  public canRegister(): boolean {
    return !!this.username && !!this.password;
  }

  // ==================================================================================================================================
  // Role Management
  // ==================================================================================================================================


  public hasRole(role: Role) {
    if (!this.roles) this.roles = [];
    return this.getRoleIndex(role) >= 0;
  }

  public addRole(role: Role) {
    if (!this.hasRole(role)) {
      this.roles.push(role);
      this.sortRoles();
    }
  }

  public removeRole(role: Role) {
    if (this.hasRole(role)) {
      this.roles.splice(this.getRoleIndex(role), 1);
      this.sortRoles();
    }
  }

  private getRoleIndex(role: Role) {
    return this.roles.findIndex(value => value.name === role.name);
  }

  // Sorting by Name
  private sortRoles() {
    this.roles.sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0);
  }

  // ==================================================================================================================================
  // Test Data
  // ==================================================================================================================================

  public setDefaultValues() {
    this.firstName = 'Laurel';
    this.lastName = 'Fishburne';
    this.username = 'TR_Pill';
    this.email = 'TR_Pill@test.de';

    this.roles = [
      new Role().setDefaultValues(),
    ];

    return this;
  }
}

