export class WaitFor {
  public async waitForThenDo(context, field, callback) {
    while (!context[field]) {
      await this.delay(1000);
    }
    callback();
  }

  public delay(timer) {
    return new Promise(resolve => {
      timer = timer || 2000;
      setTimeout(function () {
        resolve();
      }, timer);
    });
  }
}
