import {Component, OnInit, ViewChild} from '@angular/core';
import {ContainerService} from '../api/services/container/container.service';
import {ContainerModel} from '../api/model/containerModel';
import {MenuBarService} from '../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {
  @ViewChild('newContainer') private newContainer;
  public readonly CONTAINER_ACTIONS = [
    {title: '', text: 'Repository Selection', method: '/home'},
    {title: '', text: 'Asset Container', method: '/container', current: true},
    {title: '', text: 'Users', method: '/users'},
  ];

  public containers: ContainerModel[];

  constructor(private menuBarService: MenuBarService,
              private containerService: ContainerService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.CONTAINER_ACTIONS);
    this.getAllContainer();
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllContainer() {
    this.containerService.getAll().then(res => {
      this.containers = res;
    });
  }
}
