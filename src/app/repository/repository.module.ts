import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RepositoryComponent} from './repository.component';
import {RouterModule, Routes} from '@angular/router';
import {
  MatButtonModule,
  MatListModule,
  MatTooltipModule
} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../shared/shared.module';
import {AssetModule} from './asset/asset.module';
import {TaskListModule} from './task-list/task-list.module';
import {RequestModule} from './request/request.module';
import {SettingsModule} from './settings/settings.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const ROUTES: Routes = [
  {
    path: '',
    component: RepositoryComponent,
  },
  {
    path: 'asset',
    loadChildren: () => {
      return AssetModule;
    }
  },
  {
    path: 'taskList',
    loadChildren: function () {
      return TaskListModule;
    },
  },
  {
    path: 'request',
    loadChildren: function () {
      return RequestModule;
    },
  },

  {
    path: 'settings',
    loadChildren: function () {
      return SettingsModule;
    },
  },
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    MatTooltipModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [RepositoryComponent]
})
export class RepositoryModule {
}
