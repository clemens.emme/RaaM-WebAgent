import {Component, OnInit, ViewChild} from '@angular/core';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {TaskType} from '../../api/model/task-type';
import {TaskTypeService} from '../../api/services/task-type/task-type.service';
import {DefaultTaskTypesModalComponent} from '../../shared/modals/default-task-types-modal/default-task-types-modal.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild('defaultTaskTypesModal') public dfaultTaskTypesModal: DefaultTaskTypesModalComponent;
  public readonly SETTINGS_ACTIONS = [
    {title: '', text: 'Task List', method: '/repository/taskList'},
    {title: '', text: 'Request/Create Task', method: '/repository/request'},
    {title: '', text: 'Setting', method: '/repository/settings', current: true},
  ];

  public taskTypes: TaskType[];
  public displayTaskType: TaskType;

  public name: string;
  public color: string;
  public canLinkFile: boolean;

  constructor(private menuBarService: MenuBarService,
              private taskTypeService: TaskTypeService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.SETTINGS_ACTIONS);
    this.getAllTaskTypes();
  }

  // ==================================================================================================================================
  // Test
  // ==================================================================================================================================

  public resetValues() {
    this.name = '';
    this.color = '';
    this.canLinkFile = false;
  }

  public setTaskType(taskType: TaskType) {
    this.displayTaskType = taskType;
  }

  public resetTaskType() {
    this.displayTaskType = undefined;
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllTaskTypes() {
    this.taskTypeService.getAll().then(res => {
      this.taskTypes = res;
    });
  }

  protected postOneTaskType() {
    this.taskTypeService.postOne(new TaskType().setValues(this.name, this.color, this.canLinkFile)).then(res => {
      this.resetValues();
      this.getAllTaskTypes();
    });
  }

  protected putOneTaskType() {
    this.taskTypeService.putOne(this.displayTaskType).then(res => {
      this.resetTaskType();
      this.getAllTaskTypes();
    });
  }
}
