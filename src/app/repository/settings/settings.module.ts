import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings.component';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../../shared/shared.module';

const ROUTES: Routes = [
  {
    path: '',
    component: SettingsComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [SettingsComponent]
})
export class SettingsModule {
}
