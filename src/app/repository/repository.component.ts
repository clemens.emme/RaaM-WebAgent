import {Component, OnInit} from '@angular/core';
import {Repository} from '../api/model/repository';
import {environment} from '../../environments/environment';
import {GitService} from '../api/services/git/git-service.service';
import {RepositoryService} from '../api/services/repository/repository.service';
import {MenuBarService} from '../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {
  public readonly REPOSITORY_ACTIONS = [
    {title: '', text: 'Task List', method: '/repository/taskList'},
    {title: '', text: 'Request/Create Task', method: '/repository/request'},
    {title: '', text: 'Setting', method: '/repository/settings'},
  ];
  public test = environment.test;

  public history;
  public branchList;
  public fileSystem;
  public repo: Repository;

  constructor(
    public gitService: GitService,
    public repoService: RepositoryService,
    private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.REPOSITORY_ACTIONS);
    this.repoService.registerListener('Repository', () => this.getRepository());

    this.getAllInfo();
  }

  public getAllInfo() {
    this.getRepository();
    this.getGitHistory();
    this.getFileSystem();
    this.getBranchList();
  }

  public getRepository() {
    this.repo = this.repoService.getRepo();
    this.menuBarService.setDisplayRepo(this.repo);
  }

  public getGitHistory() {
    this.gitService.getOneRepoByContext().then(res => {
      this.history = res;
    });
  }

  public getFileSystem() {
    this.gitService.getOneReposFileSystemByContext().then(res => {
      this.fileSystem = res;
    });
  }

  public getBranchList() {
    this.gitService.getOneReposBranchListByContext().then(res => {
      this.branchList = res;
    });
  }

  public getFileSystemOfBranch(branch) {
    this.gitService.getOneReposFileSystemByContextAndBranch({branch: branch.short}).then(res => {
      this.fileSystem = res;
    });
  }


  public chosenCommit(commit, branch) {

  }

}
