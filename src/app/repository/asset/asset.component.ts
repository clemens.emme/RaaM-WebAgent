import {Component, OnInit} from '@angular/core';
import {GitService} from '../../api/services/git/git-service.service';
import {ParsedResponseHeaders} from 'ng2-file-upload/file-upload/file-uploader.class';

@Component({
  selector: 'app-asset',
  templateUrl: './asset.component.html',
  styleUrls: ['./asset.component.css']
})
export class AssetComponent implements OnInit {

  public gitData;
  public uploader;

  public hasBaseDropZoneOver = false;

  constructor(private gitService: GitService) {
  }

  ngOnInit() {
    this.uploader = this.gitService.getUploader();
    // this.gitData = this.gitService.getData();

    this.registerListener();

    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };

    this.uploader.onCompleteItem = (item: any, response: string, status: number, headers: ParsedResponseHeaders) => {
      // this.gitService.setDataFile(item.file.name);
    };
  }

  private registerListener() {
    this.gitService.addListener(() => {
      // this.gitData = this.gitService.getData();
    });
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
}
