import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaskListComponent} from './task-list.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TaskModule} from './task/task.module';

const ROUTES: Routes = [
  {
    path: '',
    component: TaskListComponent,
  },
  {
    path: ':id',
    loadChildren: function () {
      return TaskModule;
    },
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [TaskListComponent]
})
export class TaskListModule {
}
