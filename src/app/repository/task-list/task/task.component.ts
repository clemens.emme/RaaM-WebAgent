import {Component, OnInit} from '@angular/core';
import {Task} from '../../../api/model/task';
import {NgbProgressbarConfig} from '@ng-bootstrap/ng-bootstrap';
import {MenuBarService} from '../../../api/services/actionBar/menu-bar.service';
import {ActivatedRoute} from '@angular/router';
import {TaskService} from '../../../api/services/task/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css'],
  providers: [NgbProgressbarConfig]
})
export class TaskComponent implements OnInit {
  public readonly TASK_ACTIONS = [
    {title: '', text: 'Task List', method: '/repository/taskList'},
    {title: '', text: 'Task', method: '/repository/taskList', current: true},
    {title: '', text: 'Request/Create Task', method: '/repository/request'},
    {title: '', text: 'Setting', method: '/repository/settings'},
  ];

  public id: string;
  public repo;
  public task: Task;

  constructor(
    private route: ActivatedRoute,
    private taskService: TaskService,
    private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string '_id' to a String
    });

    this.getOneTask(this.id);
    this.menuBarService.setRoutingActions(this.TASK_ACTIONS);
  }

  public openWorkLogModal() {
    console.log('Yay its modal time!');
  }


  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getOneTask(id: string) {
    this.taskService.getOne(id).then(res => {
      console.log(res);
      this.task = res;
    });
  }

  protected deleteOneTask(id: string) {
    this.taskService.deleteOne(id).then(res => {
      console.log('deleted');
    });
  }

  // ==================================================================================================================================
  //
  // Task Data Calculations
  //
  // ==================================================================================================================================

  // creates readable time out of workLogs
  createReadableTimeParam(days: number, hours: number, minutes: number) {
    const timeContainer = {days: days, hours: hours, minutes: minutes};

    return this.createReadableTimeFrom(timeContainer);
  }

  progressInPercent(ratio) {
    if (!this.task.estimatedPoints) return '-';
    const max = ratio * this.task.estimatedPoints;
    let spend = 0;

    // loop workLogs
    if (this.task.workLogs) {
      for (const work of this.task.workLogs) {
        spend += work['hoursSpend'];
        spend += work['minutesSpend'] / Task.MINUTE;
      }
    }

    return Math.floor((spend * 100) / max);
  }

  // ratio in hours
  calcEstimatedTime(ratio) {
    const timeConteiner = {days: 0, hours: 0, minutes: 0};
    const timeValue = ratio * this.task.estimatedPoints;

    // calc hours rounded
    timeConteiner.hours = Math.floor(timeValue);
    // calc minutes rounded (seconds are dropped)
    timeConteiner.minutes = Math.floor((timeValue - timeConteiner.hours) * Task.MINUTE);

    return this.createReadableTimeFrom(timeConteiner);
  }

  // creates readable time out of workLogs
  createReadableTime() {
    if (!this.task.workLogs) return '-';
    const timeContainer = {days: 0, hours: 0, minutes: 0};

    // Sum up all values from workLogs-Entries
    if (this.task.workLogs) {
      for (const work of this.task.workLogs) {
        timeContainer.hours += work['hoursSpend'];
        timeContainer.minutes += work['minutesSpend'];
      }
    }

    return this.createReadableTimeFrom(timeContainer);
  }

  // creates readable time out if timeContainer given
  createReadableTimeFrom(timeContainer) {
    let answer = '';

    // calculate the carrier
    timeContainer.hours += Math.floor(timeContainer.minutes / Task.MINUTE); // rounding
    timeContainer.minutes = timeContainer.minutes % Task.MINUTE;

    timeContainer.days += Math.floor(timeContainer.hours / Task.HOURS); // rounding
    timeContainer.hours = timeContainer.hours % Task.HOURS;

    if (timeContainer.days > 0) answer += timeContainer.days + 'd ';
    if (timeContainer.hours > 0) answer += timeContainer.hours + 'h ';
    if (timeContainer.minutes > 0) answer += timeContainer.minutes + 'm';

    return answer;
  }


  public toLocaleString(time: string): string {
    const options = {year: 'numeric', month: '2-digit', day: '2-digit'};
    const date =  new Date(time);
    return date.toLocaleDateString('de-DE', options);
  }
}
