import {Component, OnDestroy, OnInit} from '@angular/core';
import {Task} from '../../api/model/task';
import {Repository} from '../../api/model/repository';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {RepositoryService} from '../../api/services/repository/repository.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit, OnDestroy {
  public readonly TASK_LIST_ACTIONS = [
    {title: '', text: 'Task List', method: '/repository/taskList', current: true},
    {title: '', text: 'Request/Create Task', method: '/repository/request'},
    {title: '', text: 'Setting', method: '/repository/settings'},
  ];
  public test = true;

  public repo: Repository;
  public originTaskList: Task[];

  constructor(
    private menuBarService: MenuBarService,
    private repoService: RepositoryService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.TASK_LIST_ACTIONS);
    this.repoService.registerListener('TaskList', () => this.updateList());

    this.updateList();
    this.repoService.getOneByName(this.repo.name);
  }

  ngOnDestroy() {
    this.repoService.unregisterListener('TaskList');
  }

  public updateList() {
    this.repo = this.repoService.getRepo();
    this.originTaskList = this.repo.tasks;
  }
}
