import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './request.component';
import {SharedModule} from '../../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const ROUTES: Routes = [
  {
    path: '',
    component: RequestComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [RequestComponent]
})
export class RequestModule { }
