import {Component, OnInit, ViewChild} from '@angular/core';
import {Task} from '../../api/model/task';
import {Person} from '../../api/model/person';
import {environment} from '../../../environments/environment';
import {ContainerModel} from '../../api/model/containerModel';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {NewTaskModalComponent} from '../../shared/modals/new-task-modal/new-task-modal.component';
import {AssignmentCollapsibleComponent} from '../../shared/collapsible/assignment-collapsible/assignment-collapsible.component';
import {BranchNameCollapsibleComponent} from '../../shared/collapsible/branch-name-collapsible/branch-name-collapsible.component';
import {FilePathCollapsibleComponent} from '../../shared/collapsible/file-path-collapsible/file-path-collapsible.component';
import {ContainerCollapsibleComponent} from '../../shared/collapsible/container-collapsible/container-collapsible.component';
import {WaitFor} from '../../utils/wait-for';
import {AssigneeCollapsibleComponent} from '../../shared/collapsible/assignee-collapsible/assignee-collapsible.component';
import {ParentCollapsibleComponent} from '../../shared/collapsible/parent-collapsible/parent-collapsible.component';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent extends WaitFor implements OnInit {
  @ViewChild('newTask') private newTask: NewTaskModalComponent;
  public readonly REQUEST_SECRET_ACTIONS = [
    {text: 'reset', call: () => this.resetTestTask()},
    {text: 'default', call: () => this.setDefaultValues()},
    {text: 'close all', call: () => this.changeAllCollapsible(false)},
    {text: 'alohomora', call: () => this.openAllCollapsible()},
  ];
  public readonly REQUEST_ACTIONS = [
    {title: '', text: 'Task List', method: '/repository/taskList'},
    {title: '', text: 'Request/Create Task', method: '/repository/request', current: true},
    {title: '', text: 'Setting', method: '/repository/settings'},
  ];

  public test = environment.test;

  // ==================================================== Assignment Variables ========================================================== //
  @ViewChild('assignment') assignmentCollapsible: AssignmentCollapsibleComponent;
  public showAssignment: boolean;
  public task = new Task();

  // =================================================== Branch Name Variables ========================================================== //
  @ViewChild('branch') branchCollapsible: BranchNameCollapsibleComponent;
  public showBranchName: boolean;
  public branchName: string;

  // ====================================================== File Variables ============================================================== //
  @ViewChild('pathFile') pathCollapsible: FilePathCollapsibleComponent;
  public showFilePath: boolean;
  public path: string;

  // ==================================================== Container Variables =========================================================== //
  @ViewChild('containerr') containerCollapsible: ContainerCollapsibleComponent;
  public showContainer: boolean;
  public container: ContainerModel;
  public parameters: any;

  // ===================================================== Assignee Variables =========================================================== //
  @ViewChild('person') assigneeCollapsible: AssigneeCollapsibleComponent;
  public showAssignee: boolean;
  public assignee: Person;

  // ====================================================== Parent Variables ============================================================ //
  @ViewChild('parent') parentCollapsible: ParentCollapsibleComponent;
  public showParent: boolean;

  // ===================================================== Children Variables =========================================================== //
  public showChildren: boolean;

  constructor(private menuBarService: MenuBarService) {
    super();
  }

  ngOnInit() {
    this.setSecret();
    this.menuBarService.setRoutingActions(this.REQUEST_ACTIONS);
  }

  public setSecret() {
    this.menuBarService.setSecretActions(this.REQUEST_SECRET_ACTIONS);
  }

  // ==================================================================================================================================
  // Assignment Methods
  // ==================================================================================================================================

  public assignmentFinished(): boolean {
    return this.task && this.task.type && !!this.task.title;
  }

  // ==================================================================================================================================
  // Branch Methods
  // ==================================================================================================================================

  public displayBranch(): boolean {
    return this.assignmentFinished() && this.task.type.canLinkFile;
  }

  // ==================================================================================================================================
  // File Methods
  // ==================================================================================================================================

  public displayFilePath(): boolean {
    return this.displayBranch() && !!this.branchName;
  }

  public isExistingFileEndpoint() {
    if (!this.path) return false;
    return !this.path.endsWith('/');
  }

  // ==================================================================================================================================
  // Container Methods
  // ==================================================================================================================================

  public displayContainer(): boolean {
    return this.displayFilePath() && !this.isExistingFileEndpoint();
  }

  // ==================================================================================================================================
  // Assignee Methods
  // ==================================================================================================================================

  public displayAssignee(): boolean {
    return this.assignmentFinished();
  }

  // ==================================================================================================================================
  // Parent Methods
  // ==================================================================================================================================

  public displayParent(): boolean {
    return this.assignmentFinished();
  }

  // ==================================================================================================================================
  // Children Methods
  // ==================================================================================================================================

  public displayChildren(): boolean {
    return this.assignmentFinished();
  }

  // ==================================================================================================================================
  // Execution Methods
  // ==================================================================================================================================

  public displayExecution(): boolean {
    return this.assignmentFinished();
  }

  protected open() {
    // if (this.task.type.canLinkFile && this.branchName && (!this.parameters || !this.parameters.name)) return;

    // TODO check if task is complete and valid

    this.newTask.open(this.buildTask(), this.parameters);
  }

  protected buildTask(): Task {
    const taskToCreate = new Task();

    // Assignment
    taskToCreate.type = this.task.type;
    taskToCreate.title = this.task.title;
    taskToCreate.estimatedPoints = this.task.estimatedPoints;
    taskToCreate.description = this.task.description;

    // branch, file, container
    taskToCreate.branchName = this.branchName;
    taskToCreate.filePath = this.path;
    taskToCreate.container = this.container;

    // assignee
    taskToCreate.assignee = this.assignee;

    // task relations
    taskToCreate.parent = this.task.parent;
    taskToCreate.children = this.task.children;

    return taskToCreate;
  }

  // ==================================================================================================================================
  // Testing Methods
  // ==================================================================================================================================

  public openAllCollapsible() {
    this.changeAllCollapsible(true);
  }

  private changeAllCollapsible(bool: boolean) {
    this.showAssignment = bool;
    this.showBranchName = bool;
    this.showFilePath = bool;
    this.showContainer = bool;
    this.showAssignee = bool;
    this.showParent = bool;
    this.showChildren = bool;
  }

  public resetTestTask() {
    this.setSecret();
    this.resetAllValues();
  }

  protected async setDefaultValues() {
    this.assignmentCollapsible.setDefault();

    this.waitForThenDo(this, 'branchCollapsible', () => {
      this.branchCollapsible.setDefault();
    });

    this.waitForThenDo(this, 'pathCollapsible', () => {
      this.pathCollapsible.setDefault();
    });

    this.waitForThenDo(this, 'containerCollapsible', () => {
      this.containerCollapsible.setDefault();
      this.showContainer = true;
    });

    this.waitForThenDo(this, 'assigneeCollapsible', () => {
      this.assigneeCollapsible.setDefault();
    });

  }

  protected resetAllValues() {
    this.task = new Task();
    this.assignee = undefined;
    this.container = undefined;
    this.path = '';
    this.parameters = {};
    this.changeAllCollapsible(false);
  }
}
