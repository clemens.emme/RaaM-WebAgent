import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {HttpService} from './api/services/utils/http-service.service';
import {GitService} from './api/services/git/git-service.service';

import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatInputModule, MatListModule, MatDividerModule, MatTableModule} from '@angular/material';
import {MatGridListModule, MatCardModule, MatSnackBarModule, MatTooltipModule} from '@angular/material';

import {AppComponent} from './app.component';
import {SharedModule} from './shared/shared.module';
import {RouterModule, Routes} from '@angular/router';
import {RepositoryModule} from './repository/repository.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MenuBarService} from './api/services/actionBar/menu-bar.service';
import {ContainerModule} from './container/container.module';
import {UsersModule} from './users/users.module';
import {HomeModule} from './home/home.module';
import {UserService} from './api/services/user/user.service';
import {RepositoryService} from './api/services/repository/repository.service';
import {ContainerService} from './api/services/container/container.service';
import {RoleService} from './api/services/role/role.service';
import {AppGuard} from './api/guards/app/app.guard';
import {DisplayUserGuard} from './api/guards/display-user/display-user.guard';
import {DisplayRepoGuard} from './api/guards/display-repo/display-repo.guard';
import {TaskTypeService} from './api/services/task-type/task-type.service';
import {TaskService} from './api/services/task/task.service';
import {HttpClientModule} from '@angular/common/http';

const ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: '',
    children: [
      {
        path: 'home',
        loadChildren: function () {
          return HomeModule;
        },
      },
      {
        path: 'repository',
        canActivate: [AppGuard, DisplayRepoGuard],
        canDeactivate: [DisplayRepoGuard],
        loadChildren: function () {
          return RepositoryModule;
        },
      },
      {
        path: 'container',
        canActivate: [AppGuard],
        loadChildren: function () {
          return ContainerModule;
        },
      },
      {
        path: 'users',
        canActivate: [AppGuard],
        canDeactivate: [DisplayUserGuard],
        loadChildren: function () {
          return UsersModule;
        },
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpModule,
    SharedModule,
    NgbModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    FlexLayoutModule,
    RouterModule.forRoot(ROUTES),
    NgbModule.forRoot(),
  ],
  providers: [
    AppGuard,
    DisplayUserGuard,
    DisplayRepoGuard,
    GitService,
    HttpService,
    // Database
    UserService,
    RoleService,
    TaskService,
    MenuBarService,
    TaskTypeService,
    ContainerService,
    RepositoryService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
