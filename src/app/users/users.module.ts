import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UsersComponent} from './users.component';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FlexLayoutModule} from '@angular/flex-layout';
import {SharedModule} from '../shared/shared.module';
import {UserModule} from './user/user.module';
import {UserSettingModule} from './user-settings/user-settings.module';
import {UserCreationModule} from './user-creation/user-creation.module';
import {DisplayUserGuard} from '../api/guards/display-user/display-user.guard';

const ROUTES: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [DisplayUserGuard],
  },
  {
    path: 'add',
    loadChildren: () => {
      return UserCreationModule;
    }
  },
  {
    path: 'settings',
    loadChildren: () => {
      return UserSettingModule;
    }
  },
  { // keep id's always on bottom of the routes hook
    path: ':id',
    loadChildren: () => {
      return UserModule;
    }
  },
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [UsersComponent]
})
export class UsersModule {
}
