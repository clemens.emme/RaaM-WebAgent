import {Component, OnInit} from '@angular/core';
import {UserService} from '../api/services/user/user.service';
import {Person} from '../api/model/person';
import {MenuBarService} from '../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public readonly USERS_ACTIONS = [
    {title: '', text: 'Repository Selection', method: '/home'},
    {title: '', text: 'Asset Container', method: '/container'},
    {title: '', text: 'Users', method: '/users', current: true},
  ];

  public users: Person[];

  constructor(private userService: UserService,
              private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.USERS_ACTIONS);
    this.getAllUsers();
  }

  getAllUsers() {
    this.userService.getAll().then(res => {
      this.users = res;
    });
  }
}
