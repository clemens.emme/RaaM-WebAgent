import {Component, OnInit, ViewChild} from '@angular/core';
import {Role} from '../../api/model/role';
import {RoleService} from '../../api/services/role/role.service';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {DefaultRolesModalComponent} from '../../shared/modals/default-roles-modal/default-roles-modal.component';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {
  @ViewChild('defaultRolesModal') public defaultRolesModal: DefaultRolesModalComponent;
  public readonly USER_SETTINGS_ACTIONS = [
    {title: '', text: 'User Creation', method: 'users/add'},
    {title: '', text: 'User Settings', method: 'users/settings', current: true},
  ];

  public roles: Role[];
  public displayRole: Role;

  public name: string;
  public color: string;

  constructor(private roleService: RoleService,
              private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.USER_SETTINGS_ACTIONS);
    this.getAllRolls();
  }

  // ==================================================================================================================================
  // Test
  // ==================================================================================================================================

  public resetValues() {
    this.name = '';
    this.color = '';
  }

  public setRole(role: Role) {
    this.displayRole = role;
  }

  public resetRole() {
    this.displayRole = undefined;
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllRolls() {
    this.roleService.getAll().then(res => {
      this.roles = res;
    });
  }

  protected postOneRoll() {
    this.roleService.postOne(new Role().setValues(this.name, this.color)).then(res => {
      this.resetValues();
      this.getAllRolls();
    });
  }

  protected putOneRoll() {
    this.roleService.putOne(this.displayRole).then(res => {
      this.resetRole();
      this.getAllRolls();
    });
  }
}
