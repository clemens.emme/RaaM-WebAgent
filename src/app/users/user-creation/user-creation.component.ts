import { Component, OnInit } from '@angular/core';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';

@Component({
  selector: 'app-user-creation',
  templateUrl: './user-creation.component.html',
  styleUrls: ['./user-creation.component.css']
})
export class UserCreationComponent implements OnInit {
  public readonly USER_CREATION_ACTIONS = [
    {title: '', text: 'User Creation', method: 'users/add' , current: true},
    {title: '', text: 'User Settings', method: 'users/settings'},
  ];

  constructor(private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.menuBarService.setRoutingActions(this.USER_CREATION_ACTIONS);
  }

}
