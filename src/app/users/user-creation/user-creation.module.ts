import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserCreationComponent} from './user-creation.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

const ROUTES: Routes = [
  {
    path: '',
    component: UserCreationComponent,
  }
];

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FlexLayoutModule,
    SharedModule,
    RouterModule.forChild(ROUTES),
  ],
  declarations: [UserCreationComponent]
})
export class UserCreationModule {
}
