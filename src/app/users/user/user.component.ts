import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Person} from '../../api/model/person';
import {UserService} from '../../api/services/user/user.service';
import {Role} from '../../api/model/role';
import {RoleService} from '../../api/services/role/role.service';
import {ActivatedRoute} from '@angular/router';
import {MenuBarService} from '../../api/services/actionBar/menu-bar.service';
import {DefaultRolesModalComponent} from '../../shared/modals/default-roles-modal/default-roles-modal.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  @ViewChild('defaultRolesModal') public defaultRolesModal: DefaultRolesModalComponent;
  public readonly USER_ACTIONS = [
    {title: '', text: 'User Creation', method: 'users/add'},
    {title: '', text: 'User Settings', method: 'users/settings'},
  ];

  public id: string;
  public user: Person;
  public roles: Role[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private roleService: RoleService,
    private menuBarService: MenuBarService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string '_id' to a String
    });

    this.getOneUser(this.id);
    this.getAllRolls();
    this.menuBarService.setRoutingActions(this.USER_ACTIONS);
  }

  ngOnDestroy() {
    this.putOneUser();
  }


  // ==================================================================================================================================
  // Data Manipulation Methods
  // ==================================================================================================================================


  public changeRoll(changeValue: boolean, roll: Role) {
    // if true add, else remove
    if (changeValue) {
      this.user.addRole(roll);
    } else {
      this.user.removeRole(roll);
    }
  }

  // ==================================================================================================================================
  // REST
  // ==================================================================================================================================

  protected getAllRolls() {
    this.roleService.getAll().then(res => {
      this.roles = res;
    });
  }

  protected getOneUser(id) {
    this.userService.getOne(id).then(res => {
      this.user = res;
      this.menuBarService.setDisplayUser(this.user);
    });
  }

  protected putOneUser() {
    this.userService.putOne(this.user);
  }
}
